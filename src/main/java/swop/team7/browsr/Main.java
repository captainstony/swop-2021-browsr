package swop.team7.browsr;

/**
 * Main is the entry point from where Browsr is started.
 */
public class Main {
    private final Window window;
    private final Context context;

    public Main() {
        this.window = new Window(this::initialize);
        this.context = new Context(window);
    }

    /**
     * Start Browsr.
     *
     * @param args Input arguments.
     */
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Main().window::show);
    }

    /**
     * Initialize all objects necessary to start Browsr.
     */
    private void initialize() {
        BrowserController controller = new BrowserController(this.window, this.context);
        controller.showModal();
    }
}
