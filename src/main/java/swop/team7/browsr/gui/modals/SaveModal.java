package swop.team7.browsr.gui.modals;

import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.gui.widgets.*;

import java.util.ArrayList;

/**
 * SaveModal is the modal instance for the dialog screen used to save pages.
 */
public class SaveModal extends Modal {
    private final InputField filenameInput;
    private final Button saveButton;
    private final Button cancelButton;

    /**
     * Creates a new modal titled "Save As" which shows a dialog asking the user for a file name.
     */
    public SaveModal(WidgetContext context) {
        super(context);
        this.filenameInput = new InputField(getContext());
        this.saveButton = new Button(getContext(), "Save");
        this.cancelButton = new Button(getContext(), "Cancel");

        ArrayList<Button> buttons = new ArrayList<>();
        buttons.add(cancelButton);
        buttons.add(saveButton);

        Dialog dialog = new Dialog(
                getContext(),
                "Save As",
                createInputLayout("File name: ", this.filenameInput),
                buttons,
                new Size(400, 300)
        );

        setChild(dialog);
    }

    /**
     * Get the input field used to input the file name of the page to be saved.
     *
     * @return The input field for the file name of the page to be saved.
     */
    public InputField getFilenameInput() {
        return filenameInput;
    }

    /**
     * Get the button used to submit the page for saving.
     *
     * @return The button for submitting the page for saving.
     */
    public Button getSaveButton() {
        return saveButton;
    }

    /**
     * Get the button used to cancel the saving of the page.
     *
     * @return The button for canceling the saving of the page.
     */
    public Button getCancelButton() {
        return cancelButton;
    }

    /**
     * Helper method used to create widget consisting of a label followed by an input field.
     *
     * @param label The label to be included in the widget.
     * @param field The input field to be set in the widget.
     * @return A widget with the given label followed by the given input field.
     */
    private Widget createInputLayout(String label, InputField field) {
        return new LinearLayout(getContext(), Orientation.Horizontal, 10)
                .with(new Text(getContext(), label))
                .with(field);
    }
}
