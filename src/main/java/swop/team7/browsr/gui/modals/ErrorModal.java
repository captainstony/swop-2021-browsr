package swop.team7.browsr.gui.modals;

import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.gui.widgets.Button;
import swop.team7.browsr.gui.widgets.Dialog;
import swop.team7.browsr.gui.widgets.Modal;
import swop.team7.browsr.gui.widgets.Text;

import java.util.ArrayList;

/**
 * ErrorModal is the modal instance for the dialog screen used to convey an error.
 */
public class ErrorModal extends Modal {
    private final Button confirmButton;

    public ErrorModal(WidgetContext context, String message) {
        super(context);

        this.confirmButton = new Button(getContext(), "Ok");

        ArrayList<Button> buttons = new ArrayList<>();
        buttons.add(confirmButton);

        Dialog dialog = new Dialog(
                getContext(),
                "Error",
                new Text(getContext(), message),
                buttons,
                new Size(500, 150)
        );

        setChild(dialog);
    }

    /**
     * Get the button used to confirm the error.
     *
     * @return The button for confirming the error.
     */
    public Button getConfirmButton() {
        return confirmButton;
    }
}
