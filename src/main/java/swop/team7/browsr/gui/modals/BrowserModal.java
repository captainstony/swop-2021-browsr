package swop.team7.browsr.gui.modals;

import swop.team7.browsr.focus.FrameFocusManager;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.gui.widgets.*;
import swop.team7.browsr.handlers.DocumentViewHandler;
import swop.team7.browsr.handlers.ShowAddBookmarkDialogHandler;
import swop.team7.browsr.handlers.ShowSaveDialogHandler;
import swop.team7.browsr.handlers.ShowWelcomePageHandler;

/**
 * BrowserModal is the modal instance for the main browser.
 */
public class BrowserModal extends Modal implements DocumentViewHandler {
    private final AddressBar addressBar;
    private final Container documentViewContainer;
    private final BookmarkBar bookmarkBar;
    private ShowSaveDialogHandler showSaveDialogHandler;
    private ShowAddBookmarkDialogHandler showAddBookmarkDialogHandler;
    private ShowWelcomePageHandler showWelcomePageHandler;

    public BrowserModal(WidgetContext context) {
        super(context);
        this.addressBar = new AddressBar(getContext());
        this.bookmarkBar = new BookmarkBar(getContext());
        this.documentViewContainer = new Container(getContext());

        LinearLayout layout = new LinearLayout(getContext(), Orientation.Vertical)
                .with(this.addressBar)
                .with(this.bookmarkBar)
                .with(this.documentViewContainer);

        this.setChild(layout);
    }

    /**
     * Register the given ShowDialogHandler as the ShowDialogHandler for this modal.
     *
     * @param handler The handler to be registered.
     */
    public void registerShowSaveDialogHandler(ShowSaveDialogHandler handler) {
        this.showSaveDialogHandler = handler;
    }

    /**
     * Register the given AddBookmarkHandler as the AddBookmarkHandler for this modal.
     *
     * @param handler The handler to be registered.
     */
    public void registerShowAddBookMarkDialogHandler(ShowAddBookmarkDialogHandler handler) {
        this.showAddBookmarkDialogHandler = handler;
    }

    /**
     * Register the given ShowWelcomePageHandler as the ShowWelcomePageHandler for this modal.
     *
     * @param handler The handler to be registered.
     */
    public void registerShowWelcomePageHandler(ShowWelcomePageHandler handler) {
        this.showWelcomePageHandler = handler;
    }

    /**
     * Get the address bar of this modal.
     *
     * @return The address bar of this modal.
     */
    public AddressBar getAddressBar() {
        return this.addressBar;
    }

    /**
     * Get the bookmark bar of this modal.
     *
     * @return The bookmark bar for this modal.
     */
    public BookmarkBar getBookmarkBar() {
        return this.bookmarkBar;
    }

    /**
     * Get the container with the document view widget tree representing the document view of this modal.
     *
     * @return The container with the document view widget tree representing the document view of this modal.
     */
    public Container getDocumentViewContainer() {
        return documentViewContainer;
    }

    /**
     * Get the root of the document view widget tree representing the document of this modal.
     *
     * @return The root of the document view widget tree representing the document of this modal.
     */
    public DocumentViewWidget getDocumentViewRoot() {
        return (DocumentViewWidget) (documentViewContainer.getChild());
    }

    /**
     * Set the root of the document view widget tree representing the document of this modal to the given document view widget.
     *
     * @param newRoot The root of the document view widget tree representing the document of this modal.
     */
    public void setDocumentViewRoot(DocumentViewWidget newRoot) {
        this.documentViewContainer.setChild((Widget) newRoot);
    }

    /**
     * Add a bookmark to the bookmark bar of this modal.
     *
     * @param bookmark The bookmark to be added.
     */
    public void addBookmark(Link bookmark) {
        this.bookmarkBar.addBookmark(bookmark);
    }

    @Override
    public void handleKeyEvent(KeyEvent e) {
        super.handleKeyEvent(e);

        if (e.getModifier() == Modifier.CTRL && e.getType() == KeyEvent.Type.PRESSED) {
            switch (e.getCode()) {
                case 83 /*s*/ -> this.showSaveDialogHandler.showSaveDialog();
                case 68 /*d*/ -> this.showAddBookmarkDialogHandler.showAddBookmarkDialog();
                case 72 /*h*/ -> FrameFocusManager.getFocussed().handleKeyEvent(e);
                case 86 /*v*/ -> FrameFocusManager.getFocussed().handleKeyEvent(e);
                case 88 /*x*/ -> FrameFocusManager.getFocussed().handleKeyEvent(e);
            }
        }
    }

    @Override
    public void handleCloseFrame(Frame closed) {
        showWelcomePageHandler.showWelcomePage();
    }

    @Override
    public void handleSplitFrame(Frame splitted, Orientation or) {
        Frame newFrame = new Frame(getContext());
        newFrame.registerUrlChangedHandler(splitted.getUrlChangedHandler());
        newFrame.registerSwitchFrameHandler(splitted.getSwitchFrameHandler());

        Split newSplit = new Split(getContext(), or, splitted, newFrame);
        newSplit.registerDocumentViewHandler(this);

        splitted.registerDocumentViewHandler(newSplit);
        newFrame.registerDocumentViewHandler(newSplit);

        setDocumentViewRoot(newSplit);

        newFrame.loadDocument(splitted.getUrl());
    }

    @Override
    public void handleCollapseSplit(Split collapsed, DocumentViewWidget child) {
        setDocumentViewRoot(child);
        child.registerDocumentViewHandler(this);
        child.receiveFocus();
        getContext().requestLayout();
        getContext().requestRepaint();
    }
}

