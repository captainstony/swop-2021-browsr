package swop.team7.browsr.gui.modals;

import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.gui.widgets.*;

import java.util.ArrayList;

/**
 * AddBookmarkModal is the modal instance for the dialog screen used to add bookmarks.
 */
public class AddBookmarkModal extends Modal {
    private final InputField nameInput;
    private final InputField urlInput;
    private final Button submitButton;
    private final Button cancelButton;

    /**
     * Creates a new modal titled "Add Bookmark" that shows a dialog asking the user for a name and a url.
     *
     * @param url The initial value of the url field.
     */
    public AddBookmarkModal(WidgetContext context, String url) {
        super(context);
        nameInput = new InputField(context);
        urlInput = new InputField(context);
        if (url != null) {
            urlInput.setText(url);
        }
        submitButton = new Button(context, "Add Bookmark");
        cancelButton = new Button(context, "Cancel");

        ArrayList<Button> buttons = new ArrayList<>();
        buttons.add(cancelButton);
        buttons.add(submitButton);

        LinearLayout inputLayout = new LinearLayout(context, Orientation.Vertical, 10)
                .with(createInputLayout("Name", nameInput))
                .with(createInputLayout("URL ", urlInput));

        Dialog dialog = new Dialog(
                getContext(),
                "Add Bookmark",
                inputLayout,
                buttons,
                new Size(700, 300)
        );

        this.setChild(dialog);
    }

    /**
     * Helper method used to create widget consisting of a label followed by an input field.
     *
     * @param label The label to be included in the widget.
     * @param field The input field to be set in the widget.
     * @return A widget with the given label followed by the given input field.
     */
    private Widget createInputLayout(String label, InputField field) {
        return new LinearLayout(getContext(), Orientation.Horizontal)
                .with(new Text(getContext(), label))
                .with(field);
    }

    /**
     * Get the input field used to input the name of the bookmark to be added.
     *
     * @return The input field for the name of the bookmark to be added.
     */
    public InputField getNameInput() {
        return nameInput;
    }

    /**
     * Get the input field used to input the url for the bookmark.
     *
     * @return The input field for the url of the bookmark.
     */
    public InputField getUrlInput() {
        return urlInput;
    }

    /**
     * Get the button used to submit the the bookmark to be added.
     *
     * @return The button for submitting the bookmark to be added.
     */
    public Button getSubmitButton() {
        return submitButton;
    }

    /**
     * Get the button used to cancel the adding of the bookmark.
     *
     * @return The button for canceling the adding of the bookmark.
     */
    public Button getCancelButton() {
        return cancelButton;
    }
}
