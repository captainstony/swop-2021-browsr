package swop.team7.browsr.gui.events;

/**
 * KeyEvent represents a key input event. It is passed to
 * {@link swop.team7.browsr.gui.widgets.Widget#handleKeyEvent(KeyEvent)}.
 * This class is immutable by design, such that child widgets cannot change the object
 * that was passed from their parent.
 * <p>
 * This class also contains key codes for some special keys.
 */
public class KeyEvent {
    public static final int BACKSPACE = 8;
    public static final int ENTER = 10;
    public static final int ESCAPE = 27;
    public static final int END = 35;
    public static final int LEFT = 37;
    public static final int HOME = 36;
    public static final int RIGHT = 39;
    public static final int DELETE = 127;

    private final Type type;
    private final int code;
    private final char character;
    private final Modifier modifier;

    /**
     * Creates a new KeyEvent
     *
     * @param type      The type of the event
     * @param code      The keycode of the key that was pressed
     * @param character The character on the key that was pressed
     * @param modifier  The modifier mask that was present when the key was pressed
     */
    public KeyEvent(Type type, int code, char character, Modifier modifier) {
        this.type = type;
        this.code = code;
        this.character = character;
        this.modifier = modifier;
    }

    /**
     * Calls {@link #KeyEvent(Type, int, char, Modifier)} with fromInt applied to id and modifier
     */
    public KeyEvent(int id, int code, char character, int modifier) {
        this(Type.fromInt(id), code, character, Modifier.fromInt(modifier));
    }

    public Type getType() {
        return type;
    }

    public int getCode() {
        return code;
    }

    public char getCharacter() {
        return character;
    }

    public Modifier getModifier() {
        return this.modifier;
    }

    public enum Type {
        PRESSED,
        RELEASED,
        TYPED;

        /**
         * Creates a new Type instance from the given integer.
         */
        public static Type fromInt(int id) {
            return switch (id) {
                case 400 -> Type.TYPED;
                case 401 -> Type.PRESSED;
                case 402 -> Type.RELEASED;
                default -> null;
            };
        }
    }
}
