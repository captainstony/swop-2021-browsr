package swop.team7.browsr.gui.events;

/**
 * The modifier mask that was present during an input event
 */
public enum Modifier {
    NONE,
    SHIFT,
    CTRL,
    ALT;

    /**
     * Creates a new Modifier instance from the given integer.
     */
    public static Modifier fromInt(int modifier) {
        return switch (modifier) {
            case 0 -> Modifier.NONE;
            case 64 -> Modifier.SHIFT;
            case 128 -> Modifier.CTRL;
            case 512 -> Modifier.ALT;
            default -> null;
        };
    }
}
