package swop.team7.browsr.gui.events;

import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;

/**
 * MouseEvent represents a mouse input event. It is passed to
 * {@link swop.team7.browsr.gui.widgets.Widget#handleMouseEvent(MouseEvent)}.
 * This class is immutable by design, such that child widgets cannot change the object
 * that was passed from their parent.
 */
public class MouseEvent {
    private final Type type;
    private final Position position;
    private final int clickCount;
    private final int button;
    private final Modifier modifier;

    /**
     * Creates a new MouseEvent
     *
     * @param type       The type of the event
     * @param position   The position of the mouse cursor when the event occurred
     * @param clickCount The amount of times the mouse was clicked
     * @param button     The mouse button that was pressed
     * @param modifier   The modifier mask
     */
    public MouseEvent(Type type, Position position, int clickCount, int button, Modifier modifier) {
        this.position = position;
        this.type = type;
        this.clickCount = clickCount;
        this.button = button;
        this.modifier = modifier;
    }

    /**
     * Calls {@link #MouseEvent(Type, Position, int, int, Modifier)} with fromInt applied to id and modifier
     */
    public MouseEvent(int id, Position position, int clickCount, int button, int modifier) {
        this(Type.fromInt(id), position, clickCount, button, Modifier.fromInt(modifier));
    }

    public Type getType() {
        return type;
    }

    public Position getPosition() {
        return position;
    }

    public int getClickCount() {
        return clickCount;
    }

    public int getButton() {
        return button;
    }

    public Modifier getModifier() {
        return modifier;
    }

    /**
     * Creates a new mouse event identical to the current object, but translated to the given position
     */
    public MouseEvent translate(Position pos) {
        Position position = new Position(this.position.getX() - pos.getX(), this.position.getY() - pos.getY());
        return new MouseEvent(this.type, position, this.clickCount, this.button, this.modifier);
    }

    /**
     * Creates a new mouse event identical to the current object, but translated to the given coordinates
     */
    public MouseEvent translate(int x, int y) {
        return this.translate(new Position(x, y));
    }

    /**
     * Checks whether the current mouse event is withing the given bounds
     */
    public boolean isWithin(Size size) {
        int x = this.getPosition().getX();
        int y = this.getPosition().getY();
        return (x > 0 && y > 0 && x < size.getWidth() && y < size.getHeight());
    }

    /**
     * The type of the mouse event
     */
    public enum Type {
        PRESSED,
        RELEASED,
        CLICKED,
        DRAGGED;

        /**
         * Creates a new Type instance from the given integer.
         */
        public static Type fromInt(int type) {
            return switch (type) {
                case 500 -> Type.CLICKED;
                case 501 -> Type.PRESSED;
                case 502 -> Type.RELEASED;
                case 506 -> Type.DRAGGED;
                default -> null;
            };
        }
    }
}
