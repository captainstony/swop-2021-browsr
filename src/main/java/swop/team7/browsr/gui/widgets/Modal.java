package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.focus.KeyboardFocusManager;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.WidgetContext;


/**
 * Modal is the base class for all modals of Browsr.
 * <p>
 * Modals are special top level Containers that can be placed on a modality stack.
 * They receive all key and mouse events from the window.
 */
public abstract class Modal extends Container {
    public Modal(WidgetContext context, Widget child) {
        super(context, child);
    }

    public Modal(WidgetContext context) {
        super(context);
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);
    }

    @Override
    public void handleKeyEvent(KeyEvent e) {
        super.handleKeyEvent(e);
        KeyboardFocusManager.sendKeyEventToFocussed(e);
    }
}
