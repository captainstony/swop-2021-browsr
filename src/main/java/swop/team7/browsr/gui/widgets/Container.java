package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

/**
 * A simple widget that contains another Widget.
 * <p>
 * The child field stores this Widget. This field can be mutated.
 */
public class Container extends Widget {
    private Widget child;

    /**
     * Creates a new Container that contains the given child.
     */
    public Container(WidgetContext context, Widget child) {
        super(context);
        this.child = child;
    }

    /**
     * Creates a new Container that contains a new placeholder Widget with no functionality.
     */
    public Container(WidgetContext context) {
        super(context);
        this.child = new Widget(context) {
        };
    }

    public Widget getChild() {
        return child;
    }

    public void setChild(Widget child) {
        this.child = child;
    }

    @Override
    public void render(Graphics g) {
        super.render(g);
        child.render(g);
    }

    @Override
    public void layout(Size maxSize) {
        this.child.layout(maxSize);
        this.setSize(this.child.getSize().constrain(maxSize));
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);
        child.handleMouseEvent(e);
    }
}
