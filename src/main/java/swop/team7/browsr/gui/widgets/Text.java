package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

/**
 * Text is a Widget that displays text in a given font and color.
 */
public class Text extends Widget {
    public static int margin = 5;
    private String text;
    private Font font;
    private Color color;

    /**
     * Create a new Text widget in the given context with the given text, font and color.
     */
    public Text(WidgetContext context, String text, Font font, Color color) {
        super(context);
        this.text = text;
        this.font = font;
        this.color = color;
    }

    /**
     * Create a new Text widget in the given context with the given text.
     * It will use the default font provided by the context and use a black color.
     */
    public Text(WidgetContext context, String text) {
        this(context, text, context.getDefaultFont(), Color.BLACK);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void render(Graphics g) {
        super.render(g);
        g.setFont(font);
        g.setColor(color);
        g.drawString(text, margin, this.getContext().getTextOffset(font) + margin);
    }

    @Override
    public void layout(Size maxSize) {
        this.setSize(getContext().getTextSize(font, this.text).add(new Size(2 * margin, 2 * margin)));
    }
}
