package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * LinearLayout is a Widget that can stack Widgets either vertically or horizontally.
 */
public class LinearLayout extends Widget {
    private final ArrayList<Child> children;
    private final Orientation orientation;
    private final int padding;

    public LinearLayout(WidgetContext context, Orientation orientation, int padding) {
        super(context);
        this.orientation = orientation;
        this.children = new ArrayList<>();
        this.padding = padding;
    }

    public LinearLayout(WidgetContext context, Orientation orientation) {
        this(context, orientation, 0);
    }

    public LinearLayout(WidgetContext context, List<Widget> widgets, Orientation orientation, int padding) {
        super(context);
        this.children = widgets.stream().map(Child::new).collect(Collectors.toCollection(ArrayList::new));
        this.orientation = orientation;
        this.padding = padding;
    }

    public LinearLayout(WidgetContext context, List<Widget> widgets, Orientation orientation) {
        this(context, widgets, orientation, 0);
    }

    public ArrayList<Child> getChildren() {
        return children;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public LinearLayout with(Widget child) {
        this.children.add(new Child(child));
        return this;
    }

    @Override
    public void render(Graphics g) {
        super.render(g);

        children.forEach(child -> {
            if (this.orientation == Orientation.Vertical) {
                child.widget.render(GraphicsUtil.clone(g, new Position(0, child.offset), child.size));
            } else {
                child.widget.render(GraphicsUtil.clone(g, new Position(child.offset, 0), child.size));
            }
        });
    }

    @Override
    public void layout(Size maxSize) {
        super.layout(maxSize);

        Size childSize = maxSize;

        if (this.orientation == Orientation.Vertical) {
            childSize = childSize.setHeight(childSize.getHeight() - padding);
        } else {
            childSize = childSize.setWidth(childSize.getWidth() - padding);
        }

        int x = padding;
        int y = padding;

        for (Child child : children) {
            Widget widget = child.widget;
            widget.layout(childSize);

            child.size = widget.getSize();

            if (this.orientation == Orientation.Vertical) {
                child.offset = y;
                y += widget.getSize().getHeight() + padding;
                if (widget.getSize().getWidth() > x) {
                    x = widget.getSize().getWidth();
                }
                childSize = childSize.setHeight(childSize.getHeight() - widget.getSize().getHeight() - padding);
            } else { // if (this.direction == Direction.Horizontal)
                child.offset = x;
                x += widget.getSize().getWidth() + padding;
                if (widget.getSize().getHeight() > y) {
                    y = widget.getSize().getHeight();
                }
                childSize = childSize.setWidth(childSize.getWidth() - widget.getSize().getWidth() - padding);
            }
        }

        setSize(new Size(x, y).constrain(maxSize));
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);

        for (Child c : children) {
            Widget child = c.widget;
            MouseEvent childEvent;

            if (orientation == Orientation.Vertical) {
                childEvent = e.translate(0, c.offset);
            } else { // if (direction == Direction.Horizontal)
                childEvent = e.translate(c.offset, 0);
            }

            child.handleMouseEvent(childEvent);
        }
    }

    class Child {
        int offset = 0;
        Size size;
        Widget widget;

        Child(Widget widget) {
            this.widget = widget;
        }
    }
}
