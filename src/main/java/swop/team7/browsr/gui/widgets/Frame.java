package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.focus.FrameFocusManager;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.handlers.DocumentViewHandler;
import swop.team7.browsr.handlers.SwitchFrameHandler;
import swop.team7.browsr.handlers.UrlChangedHandler;

import java.awt.*;

/**
 * Frame is the widget representing a part of the view of the document area for Browsr.
 * Frames can be split horizontally or vertically using the Ctrl+H or Ctr+V key inputs respectively.
 * Frames can be closed using the Ctr+X key input.
 * Frames receive focus by being clicked and lose focus only when another frame receives focus.
 */
public class Frame extends Container implements DocumentViewWidget {
    private final ScrollableContainer htmlContainer;
    private String url;
    private UrlChangedHandler urlChangedHandler;
    private DocumentViewHandler documentViewHandler;
    private SwitchFrameHandler switchFrameHandler;

    /**
     * Create a new Frame that contains an empty document.
     */
    public Frame(WidgetContext context) {
        super(context);
        htmlContainer = new ScrollableContainer(context);
        htmlContainer.registerScrollBar(Orientation.Horizontal);
        htmlContainer.registerScrollBar(Orientation.Vertical);
        setChild(htmlContainer);
    }

    /**
     * Get the current url for this frame.
     *
     * @return The current url for this frame.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Set the URL of this frame to the given URL.
     *
     * @param url The new URL for this frame.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Get the UrlChangedHandler of this frame.
     *
     * @return The UrlChangedHandler of this frame.
     */
    public UrlChangedHandler getUrlChangedHandler() {
        return urlChangedHandler;
    }

    /**
     * Get the DocumentViewHandler of this frame.
     *
     * @return The DocumentViewHandler of this frame.
     */
    public DocumentViewHandler getDocumentViewHandler() {
        return documentViewHandler;
    }

    /**
     * Get the SwitchFrameHandler of this frame.
     *
     * @return The SwitchFrameHandler of this frame.
     */
    public SwitchFrameHandler getSwitchFrameHandler() {
        return switchFrameHandler;
    }

    /**
     * Get the root of the widget tree representing the document of this frame.
     *
     * @return The Root of the widget tree representing the document of this frame.
     */
    public Widget getHtmlRoot() {
        return htmlContainer.getChild();
    }

    /**
     * Set the widget tree representing the document of this frame to the widget tree with the given root.
     *
     * @param htmlRoot The new root of the widget tree representing the document of this frame.
     */
    public void setHtmlRoot(Widget htmlRoot) {
        htmlContainer.setChild(htmlRoot);
    }

    /**
     * Register the given UrlChangedHandler as urlChangedHandler for this frame.
     *
     * @param urlChangedHandler The UrlChangedHandler to be registered.
     */
    public void registerUrlChangedHandler(UrlChangedHandler urlChangedHandler) {
        this.urlChangedHandler = urlChangedHandler;
    }

    /**
     * Register the given DocumentViewHandler as documentViewHandler for this frame.
     *
     * @param documentViewHandler The DocumentViewHandler to be registered.
     */
    @Override
    public void registerDocumentViewHandler(DocumentViewHandler documentViewHandler) {
        this.documentViewHandler = documentViewHandler;
    }

    /**
     * Register the given SwitchFrameHandler as switchFrameHandler for this frame.
     *
     * @param switchFrameHandler The SwitchFrameHandler to be registered.
     */
    public void registerSwitchFrameHandler(SwitchFrameHandler switchFrameHandler) {
        this.switchFrameHandler = switchFrameHandler;
    }

    @Override
    public void receiveFocus() {
        FrameFocusManager.requestFocus(this);
    }

    @Override
    public void layout(Size maxSize) {
        htmlContainer.layout(maxSize);
        setSize(maxSize);
    }

    @Override
    public void render(Graphics g) {
        if (FrameFocusManager.isFocussed(this)) {
            g.setColor(Color.BLUE);
            g.drawRect(0, 0, this.getSize().getWidth() - 1, this.getSize().getHeight() - 1);
            g.setColor(Color.BLACK);
        }
        super.render(g);
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        if (FrameFocusManager.isFocussed(this)) {
            super.handleMouseEvent(e);
        } else if (e.isWithin(this.getSize()) && e.getType() == MouseEvent.Type.CLICKED) {
            FrameFocusManager.requestFocus(this);
            switchFrameHandler.handleSwitchFrame();
            getContext().requestRepaint();
        }
    }

    @Override
    public void handleKeyEvent(KeyEvent e) {
        super.handleKeyEvent(e);

        switch (e.getCode()) {
            case 72 /*h*/ -> documentViewHandler.handleSplitFrame(this, Orientation.Horizontal);
            case 86 /*v*/ -> documentViewHandler.handleSplitFrame(this, Orientation.Vertical);
            case 88 /*x*/ -> documentViewHandler.handleCloseFrame(this);
        }
    }

    /**
     * Load the document for the given URL in this frame.
     *
     * @param url The URL of the document to be loaded.
     */
    public void loadDocument(String url) {
        FrameFocusManager.requestFocus(this);
        urlChangedHandler.handleUrlChanged(url);
    }
}
