package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.handlers.ButtonClickHandler;

import java.awt.*;

/**
 * Button is a type of Text Widget that represents clickable buttons.
 * <p>
 * A {@Button ButtonClickHandler} can be registered to respond to click events.
 */
public class Button extends Text {
    private final String text;
    int margin = 3;
    private Boolean isPressed = false;
    private Boolean isHovered = false;
    private ButtonClickHandler clickHandler;

    public Button(WidgetContext context, String text) {
        super(context, text);
        this.text = text;
    }

    public Boolean getPressed() {
        return isPressed;
    }

    public void setPressed(Boolean pressed) {
        isPressed = pressed;
    }

    public String getText() {
        return text;
    }

    public Boolean getHovered() {
        return isHovered;
    }

    public void setHovered(Boolean hovered) {
        isHovered = hovered;
    }

    //ONLY FOR TESTING
    public ButtonClickHandler getClickHandler() {
        return this.clickHandler;
    }

    //https://stackoverflow.com/questions/21989082/drawing-dashed-line-in-java
    public void drawDashedLine(Graphics g, int x1, int y1, int x2, int y2) {
        //creates a copy of the Graphics instance
        Graphics2D g2d = (Graphics2D) g.create();
        //set the stroke of the copy, not the original
        Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{5}, 0);
        g2d.setStroke(dashed);
        g2d.drawLine(x1, y1, x2, y2);
        //gets rid of the copy
        g2d.dispose();
    }

    @Override
    public void render(Graphics g) {
        //Draw the button
        g.setColor(new Color(220, 220, 220));
        if (isHovered) {
            g.setColor(new Color(190, 190, 190));
        }
        g.fillRoundRect(0, 0, super.getSize().getWidth() - 1, super.getSize().getHeight() - 1, 10, 10);
        g.setColor(Color.black);
        g.drawRoundRect(0, 0, super.getSize().getWidth() - 1, super.getSize().getHeight() - 1, 10, 10);

        if (isPressed) {
            drawDashedLine(g, margin, margin, margin, super.getSize().getHeight() - margin - 1);
            drawDashedLine(g, margin, margin, super.getSize().getWidth() - margin, margin - 1);
            drawDashedLine(g, margin, super.getSize().getHeight() - margin - 1, super.getSize().getWidth() - margin - 1, super.getSize().getHeight() - margin - 1);
            drawDashedLine(g, super.getSize().getWidth() - margin - 1, margin, super.getSize().getWidth() - margin - 1, super.getSize().getHeight() - margin - 1);
        }

        //Render the text in the button
        g.setColor(Color.black);
        super.render(g);
    }

    public void registerClickHandler(ButtonClickHandler handler) {
        this.clickHandler = handler;
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);
        Boolean change = false;
        if (e.isWithin(this.getSize())) {
            if (!getHovered()) change = true;
            setHovered(true);
        } else {
            if (getHovered()) change = true;
            setHovered(false);
        }
        if (e.getType() == MouseEvent.Type.CLICKED) {
            if (e.isWithin(this.getSize())) {
                if (!getPressed()) change = true;
                setPressed(true);

                if (this.clickHandler != null) {
                    this.clickHandler.handleClick();
                }
            } else {
                if (getPressed()) change = true;
                setPressed(false);
            }
        }
        if (change) this.getContext().requestRepaint();

    }


    @Override
    public void layout(Size maxSize) {
        super.layout(maxSize);
        setSize(super.getSize().add(new Size(margin, margin)));
    }
}
