package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

/**
 * Widget is the base class of all GUI objects.
 * <p>
 * Each widget is initialized with a {@link WidgetContext} object. This object will provide the widget with
 * an interface to get information from the environment, like fonts, and request layout or redraw calls.
 * <p>
 * Widgets are ordered hierarchically, this means every widget can have one or more children.
 * Parent widgets can choose how they keep track of their children.
 * <p>
 * During the {@link #layout(Size)} phase widgets will set their own size. Widgets with children must
 * call the layout method of their children and can set their own size based on the sizes of their children.
 * <p>
 * During the {@link #render(Graphics)} phase widgets are provided with a {@link Graphics} object
 * which they can use to draw themselves.
 * Widgets with children must recursively call the render method of their children.
 * <p>
 * Event handlers ({@link #handleKeyEvent} and {@link #handleMouseEvent}) can be used to respond to input events.
 * Mouse events should be propagated to the children of the widget.
 */
public abstract class Widget {
    private final WidgetContext context;
    private Size size;

    /**
     * Creates a new Widget within the given context.
     */
    public Widget(WidgetContext widgetContext) {
        this.size = new Size(0, 0);
        this.context = widgetContext;
    }

    public WidgetContext getContext() {
        return context;
    }

    public Size getSize() {
        return size;
    }

    protected void setSize(Size size) {
        this.size = size;
    }

    protected void setSize(int x, int y) {
        this.size = new Size(x, y);
    }

    /**
     * Renders the widget and its children by recursively calling the render method on the children.
     * <p>
     * You may assume that this method is called after {@link #layout(Size)},
     * so you may assume {@link #getSize()} returns a non-null value.
     * This Graphics object provided to a child widget must be translated to the position and clipped to the size
     * of the child. See {@link swop.team7.browsr.gui.util.GraphicsUtil#clone(Graphics, Position, Size)}
     * and {@link java.awt.Graphics#create(int, int, int, int)}.
     *
     * @param g The Graphics object. Everything drawn using this object will be drawn relative to the widget's position.
     *          When drawing outside the bounds of this widget, the graphics will be clipped.
     */
    public void render(Graphics g) {
    }

    /**
     * Calls layout recursively on children and sets its own size.
     * <p>
     * After calling layout on a child widget, {@link #getSize()} can be used on the child to retrieve its size.
     * The sizes of the children can be used to determine the size of this widget and the placement of its children.
     * It is important that the widget sets its own size, by using {@link #setSize(Size)}.
     *
     * @param maxSize The maximum size of the widget. This can be useful, for example when a widget has to
     *                span across the entire window. This value should not be larger than the size of the parent widget.
     */
    public void layout(Size maxSize) {
    }

    /**
     * Handles a {@link MouseEvent}. If the widget has children, the event should be propagated the appropriate child
     * based on its position and size. The position of the MouseEvent must be translated to the position of the
     * child relative to this widget, similar to how Graphics objects are translated in {@link #render}.
     */
    public void handleMouseEvent(MouseEvent e) {
    }

    /**
     * Handles a {@link KeyEvent}.
     */
    public void handleKeyEvent(KeyEvent e) {
    }

}
