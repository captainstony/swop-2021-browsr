package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.GraphicsUtil;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

/**
 * Box is a special type of {@link Container} that renders a background and border around its content.
 * <p>
 * By default, the border color is black, its size is 0 and there is no background color.
 * <p>
 * Optionally, padding can be used to add additional space between the boundaries of the box and its child.
 */
public class Box extends Container {
    private int padding = 0;
    private Color backgroundColor = null;
    private Color borderColor = Color.BLACK;
    private int borderSize = 0;

    public Box(WidgetContext context, Widget child, int padding, int borderSize, Color borderColor, Color backgroundColor) {
        super(context, child);
        this.padding = padding;
        this.borderSize = borderSize;
        this.borderColor = borderColor;
        this.backgroundColor = backgroundColor;
    }

    public Box(WidgetContext context, Widget child, int padding, int borderSize, Color borderColor) {
        this(context, child, padding, borderSize, borderColor, null);
    }

    public Box(WidgetContext context, Widget child, int padding, int borderSize) {
        this(context, child, padding, borderSize, Color.BLACK);
    }

    public Box(WidgetContext context, Widget child, int padding) {
        this(context, child, padding, 0);
    }

    public Box(WidgetContext context, Widget child) {
        super(context, child);
    }

    public Box(WidgetContext context) {
        super(context);
    }

    public void setPadding(int padding) {
        this.padding = padding;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public void setBorderSize(int borderSize) {
        this.borderSize = borderSize;
    }

    @Override
    public void render(Graphics g) {
        Graphics g2 = g.create(0, 0, getSize().getWidth(), getSize().getHeight());

        if (borderSize > 0) {
            g2.setColor(borderColor);
            g2.fillRect(0, 0, getSize().getWidth(), getSize().getHeight());
        }

        if (backgroundColor != null) {
            g2.setColor(backgroundColor);
        } else {
            g2.setColor(Color.WHITE);
        }

        if (borderSize > 0 || backgroundColor != null) {
            g2.translate(borderSize, borderSize);
            g2.fillRect(0, 0, getSize().getWidth() - borderSize * 2, getSize().getHeight() - borderSize * 2);
        }

        getChild().render(GraphicsUtil.clone(g,
                new Position(padding, padding),
                getSize().add(new Size(-2 * padding, -2 * padding))
        ));
    }

    @Override
    public void layout(Size maxSize) {
        Size paddedSize = maxSize.add(new Size(-2 * padding, -2 * padding));
        getChild().layout(paddedSize);
        setSize(getChild().getSize().constrain(paddedSize).add(new Size(2 * padding, 2 * padding)));
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e.translate(padding, padding));
    }
}
