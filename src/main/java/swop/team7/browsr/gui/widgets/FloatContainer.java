package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.GraphicsUtil;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

/**
 * FloatContainer is a Container that can position its child widget relative to its borders.
 * <p>
 * The expandWidth and expandHeight booleans are used to determine if this container should
 * use all the space it is given on layout.
 * <p>
 * The horizontalFloat and verticalFloat arguments are used to determine the position of the child widget.
 * For example (Right, Center) will position the child widget centered against the right border of this container.
 */
public class FloatContainer extends Container {
    private final boolean expandWidth;
    private final boolean expandHeight;
    private final HorizontalFloatPosition horizontalFloat;
    private final VerticalFloatPosition verticalFloat;
    private Position offset = Position.ZERO;

    public FloatContainer(
            WidgetContext context,
            Widget child,
            boolean expandWidth,
            boolean expandHeight,
            HorizontalFloatPosition horizontalFloat,
            VerticalFloatPosition verticalFloat
    ) {
        super(context, child);
        this.expandWidth = expandWidth;
        this.expandHeight = expandHeight;
        this.horizontalFloat = horizontalFloat;
        this.verticalFloat = verticalFloat;
    }

    public FloatContainer(
            WidgetContext context,
            boolean expandWidth,
            boolean expandHeight,
            HorizontalFloatPosition horizontalFloat,
            VerticalFloatPosition verticalFloat
    ) {
        this(context, new Widget(context) {
        }, expandWidth, expandHeight, horizontalFloat, verticalFloat);
    }

    @Override
    public void render(Graphics g) {
        getChild().render(GraphicsUtil.clone(g,
                offset,
                getChild().getSize()
        ));
    }

    @Override
    public void layout(Size maxSize) {
        getChild().layout(maxSize);

        Size newSize = getChild().getSize();

        if (expandWidth)
            newSize = newSize.setWidth(maxSize.getWidth());
        if (expandHeight)
            newSize = newSize.setHeight(maxSize.getHeight());

        setSize(newSize.constrain(maxSize));

        int x = switch (horizontalFloat) {
            case Left -> 0;
            case Right -> getSize().getWidth() - getChild().getSize().getWidth();
            case Center -> getSize().getWidth() / 2 - (getChild().getSize().getWidth() / 2);
        };

        int y = switch (verticalFloat) {
            case Top -> 0;
            case Bottom -> getSize().getHeight() - getChild().getSize().getHeight();
            case Center -> getSize().getHeight() / 2 - (getChild().getSize().getHeight() / 2);
        };

        offset = new Position(x, y);
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e.translate(offset));
    }

    public enum HorizontalFloatPosition {
        Left,
        Center,
        Right,
    }

    public enum VerticalFloatPosition {
        Top,
        Center,
        Bottom,
    }
}
