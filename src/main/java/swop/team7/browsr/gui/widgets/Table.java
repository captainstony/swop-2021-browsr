package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.GraphicsUtil;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Table is a Widget that aligns child widgets in a grid based on a given matrix of Widgets.
 */
public class Table extends Widget {
    private final ArrayList<ArrayList<Widget>> matrix;
    private final ArrayList<Integer> widthTranslate;
    private final ArrayList<Integer> heightTranslate;

    /**
     * Upon creation we determine the size of every row and column in the table.
     *
     * @param context The context of the table widget.
     * @param matrix  A 2D matrix containing widgets for the table.
     */
    public Table(WidgetContext context, ArrayList<ArrayList<Widget>> matrix) {
        super(context);
        this.matrix = matrix;
        int size = 0;
        for (ArrayList<Widget> col : matrix) if (size < col.size()) size = col.size();
        widthTranslate = new ArrayList<Integer>(Collections.nCopies(size + 1, 0));
        heightTranslate = new ArrayList<Integer>(Collections.nCopies(matrix.size() + 1, 0));
    }

    public ArrayList<ArrayList<Widget>> getMatrix() {
        return this.matrix;
    }

    @Override
    public void render(Graphics g) {
        int i = 0;
        for (ArrayList<Widget> col : matrix) {
            i++;
            int j = 0;
            for (Widget elem : col) {
                int xShift = 0, yShift = 0;
                for (int k = 0; k < j; k++) xShift += widthTranslate.get(k);
                for (int k = 0; k < i; k++) yShift += heightTranslate.get(k);
                //g.drawLine(xShift,0,xShift,getSize().getHeight());
                //g.drawLine(0,yShift,getSize().getWidth(),yShift);
                elem.render(GraphicsUtil.clone(g, new Position(xShift, yShift), elem.getSize()));
                j++;
            }
        }
    }

    /**
     * Additionally we have to determine the size of every cell in the table.
     * First we calculate the size of every widget in the table (happens in the first double for loop).
     * Next we loop over all the widgets and determine the cell width and the row height.
     * <p>
     * This will update on a resize event.
     */
    @Override
    public void layout(Size maxSize) {
        int i = 0;
        for (ArrayList<Widget> row : matrix) { // Determine the width and height for every cell in the table.
            i++;
            int j = 0;
            for (Widget elem : row) {
                elem.layout(maxSize);
                if (widthTranslate.get(j) < elem.getSize().getWidth()) widthTranslate.set(j, elem.getSize().getWidth());
                if (heightTranslate.get(i) < elem.getSize().getHeight())
                    heightTranslate.set(i, elem.getSize().getHeight());
                j++;
            }
        }
        int w = 0, h = 0;
        for (Integer s : widthTranslate) w += s;
        for (Integer s : heightTranslate) h += s;
        setSize(new Size(w, h)); // Set own Size

    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);

        int i = 0;
        for (ArrayList<Widget> rows : matrix) {
            i++;
            int j = 0;
            for (Widget child : rows) {
                int xShift = 0, yShift = 0;
                for (int k = 0; k < j; k++) xShift += widthTranslate.get(k);
                for (int k = 0; k < i; k++) yShift += heightTranslate.get(k);
                MouseEvent childEvent = e.translate(xShift, yShift);
                child.handleMouseEvent(childEvent);
                j++;
            }
        }
    }
}
