package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.GraphicsUtil;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * BookmarkBar is a Widget which shows a list of bookmarks next to each other.
 */
public class BookmarkBar extends Widget {
    private final ArrayList<Bookmark> bookmarks;

    /**
     * Create BookmarkBar with predefined bookmarks
     */
    public BookmarkBar(WidgetContext context, ArrayList<Link> links) {
        super(context);
        this.bookmarks = links.stream().map(BookmarkBar.Bookmark::new).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Creates an empty BookmarkBar
     */
    public BookmarkBar(WidgetContext context) {
        super(context);
        this.bookmarks = new ArrayList<>();
    }

    /**
     * Get the bookmarks of this bookmark bar.
     *
     * @return A list of links corresponding with the bookmarks of this bookmark bar.
     */
    public ArrayList<Link> getBookmarks() {
        return bookmarks.stream().map(bm -> bm.getLink()).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Add a new bookmark to the bookmarkBar
     */
    public void addBookmark(Link bookmark) {
        this.bookmarks.add(new Bookmark(bookmark));
    }

    @Override
    public void layout(Size maxSize) {
        super.layout(maxSize);

        int x = 0;
        int y = 0;

        for (Bookmark bookmark : bookmarks) {
            Link link = bookmark.link;
            link.layout(maxSize);

            bookmark.offset = x;
            x += link.getSize().getWidth();
            if (link.getSize().getHeight() > y) {
                y = link.getSize().getHeight();
            }
        }

        setSize(new Size(x, y));
    }

    @Override
    public void render(Graphics g) {
        super.render(g);

        bookmarks.forEach(bookmark -> {
            bookmark.link.render(GraphicsUtil.clone(g, new Position(bookmark.offset, 0), bookmark.link.getSize()));
        });
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);

        for (BookmarkBar.Bookmark b : bookmarks) {
            Link link = b.link;
            MouseEvent childEvent = e.translate(b.offset, 0);

            link.handleMouseEvent(childEvent);
        }
    }

    class Bookmark {
        int offset = 0;
        Link link;

        Bookmark(Link link) {
            this.link = link;
        }

        Link getLink() {
            return link;
        }
    }
}
