package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.handlers.UrlChangedHandler;

/**
 * AddressBar is the text input widget that represents the address bar of the browser.
 * <p>
 * AddressBar has extra variables url and urlChangedHandler. The url variable is a string representing the last url
 * that was searched or navigated to, which will be shown in the address bar while it has not received focus. This
 * variable is instantiated upon creation with the indicative text "Type url here". The urlChangedHandler is a
 * handler to be called when a new url is searched trough the address bar.
 */
public class AddressBar extends TextInputWidget {
    private String url = "";
    private UrlChangedHandler urlChangedHandler;

    public AddressBar(WidgetContext context) {
        super(context);
        setUrl("Type url here");
    }

    /**
     * Registers the given UrlChangedHandler as the urlChangedHandler for the AddressBar.
     *
     * @param handler The handler to be registered.
     */
    public void registerUrlChangedHandler(UrlChangedHandler handler) {
        this.urlChangedHandler = handler;
    }

    /**
     * Set the url of the AddressBar to the given url.
     *
     * @param url The new url of the AddressBar to be set.
     */
    public void setUrl(String url) {
        this.url = url;
        setText(url);
    }

    @Override
    public void layout(Size maxSize) {
        super.layout(maxSize);
    }

    @Override
    public void handleCancelTextEdit() {
        setText(url);
        getContext().requestRepaint();
    }

    @Override
    public void handleConfirmTextEdit() {
        if (this.urlChangedHandler != null)
            urlChangedHandler.handleUrlChanged(getText());
    }
}