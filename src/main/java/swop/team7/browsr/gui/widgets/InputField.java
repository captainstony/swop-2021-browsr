package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

/**
 * InputField is the text input widget that represents a generic form input field.
 * <p>
 * It has an optional name field used to reference this field in a {@link Form}.
 */
public class InputField extends TextInputWidget {
    private final String name;
    private final int length;

    public InputField(WidgetContext context, String name, int length) {
        super(context);
        this.name = name;
        this.length = length;
    }

    public InputField(WidgetContext context, int length) {
        this(context, null, length);
    }

    public InputField(WidgetContext context, String name) {
        this(context, name, 200);
    }

    public InputField(WidgetContext context) {
        this(context, null);
    }

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

    @Override
    public void layout(Size maxSize) {
        super.layout(maxSize.setWidth(length));
    }
}
