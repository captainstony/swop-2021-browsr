package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.handlers.CancelTextEditHandler;
import swop.team7.browsr.handlers.ConfirmTextEditHandler;

import java.awt.*;

/**
 * TextInput widget is the base class of all that widgets that allow for text to be typed
 * <p>
 * TextInputWidget has the following variables:
 * MARGIN: integer constant representing the margin between the text in the widget and its outer frame
 * focus: boolean indicating whether the widget has received focus
 * text: string representing the text content of the widget
 * cursorPosition: integer value indicating the position of the cursor in the widget, with 0 being the leftmost and text.length() the rightmost cursor position
 * highlight: boolean representing whether a portion of text in the widget is highlighted
 * beginHighlight: integer value indicating the cursor position where the highlighted text begins
 * endHighlight: integer value indicating the the cursor position where the highlighted text ends
 * repaintHandler: handler to be called when the widget needs to be repainted
 * focusHandler: handler to be called when the widget receives or loses focus.
 */
public abstract class TextInputWidget extends Container implements ConfirmTextEditHandler, CancelTextEditHandler {
    private final TextInput textInput;
    private final ScrollableContainer scrollableContainer;

    public TextInputWidget(WidgetContext context) {
        super(context);
        TextInput text = new TextInput(context, "", context.getDefaultFont(), Color.BLACK);
        text.registerCancelTextEditHandler(this);
        text.registerConfirmTextEditHandler(this);
        this.textInput = text;
        scrollableContainer = new ScrollableContainer(context, text);
        scrollableContainer.registerScrollBar(Orientation.Horizontal);
        setChild(scrollableContainer);
    }

    @Override
    public void layout(Size maxSize) {
        super.layout(maxSize.setHeight(
                getContext().getTextHeight(getContext().getDefaultFont()) + ScrollBar.barThickness + 2 * Text.margin
        ));
        this.setSize(this.getChild().getSize().constrain(maxSize));
    }

    @Override
    public void render(Graphics g) {
        g.drawRect(0, 0, getSize().getWidth(), getSize().getHeight());
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, getSize().getWidth(), getSize().getHeight());
        getChild().render(g);
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        if (e.getType() == MouseEvent.Type.CLICKED) {
            if (e.isWithin(this.getSize())) {
                this.getTextInput().handleClickedIn();
            } else {
                this.getTextInput().handleClickedOut();
            }
        }

        scrollableContainer.handleMouseEvent(e);
    }

    public String getText() {
        return textInput.getText();
    }

    public void setText(String textInput) {
        this.textInput.setText(textInput);
    }

    public TextInput getTextInput() {
        return this.textInput;
    }

    @Override
    public void handleCancelTextEdit() {

    }

    @Override
    public void handleConfirmTextEdit() {

    }
}
