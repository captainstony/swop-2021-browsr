package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

/**
 * ConstrainedContainer is a {@link Container} that constrains it's size to the given size regardless of the size
 * of its child..
 */
public class ConstrainedContainer extends Container {
    private final Size maxSize;

    public ConstrainedContainer(WidgetContext context, Widget child, Size size) {
        super(context, child);
        this.maxSize = size;
    }

    public ConstrainedContainer(WidgetContext context, Size size) {
        this(context, new Widget(context) {
        }, size);
    }

    @Override
    public void layout(Size maxSize) {
        super.layout(maxSize.constrain(this.maxSize));
    }
}
