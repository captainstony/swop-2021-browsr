package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.*;
import swop.team7.browsr.handlers.DocumentViewHandler;

import java.awt.*;

/**
 * This widget splits into two other widgets separated by a draggable separator.
 * <p>
 * The position of the separator is stored as a fraction of the width or height of the split, based on the direction.
 * <p>
 * The sizes stored in this split represent the maximum size each child may use.
 * It does not necessarily reflect the actual size of the child.
 */
public class Split extends Widget implements DocumentViewWidget, DocumentViewHandler {
    private final int SEPARATOR_WIDTH = 10;
    private final Orientation orientation;
    private double separatorFraction = 0.5;
    private DocumentViewWidget first;
    private DocumentViewWidget second;
    private Size firstSize;
    private Size secondSize;
    private DocumentViewHandler documentViewHandler;

    /**
     * Creates a new Split within the given context, with the given children and in the given direction
     */
    public Split(WidgetContext widgetContext, Orientation orientation, DocumentViewWidget first, DocumentViewWidget second) {
        super(widgetContext);
        this.orientation = orientation;
        this.first = first;
        this.second = second;
    }

    /**
     * Register the given DocumentViewHandler as the documentViewHandler for this split widget.
     *
     * @param documentViewHandler The DocumentViewHandler to be registered.
     */
    @Override
    public void registerDocumentViewHandler(DocumentViewHandler documentViewHandler) {
        this.documentViewHandler = documentViewHandler;
    }

    /**
     * Get the first child of this split widget.
     *
     * @return The first child of this split widget.
     */
    public DocumentViewWidget getFirst() {
        return first;
    }

    /**
     * Get the orientation of this split widget.
     *
     * @return The orientation of this split widget.
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * Returns the other child if the given child is one of the children.
     * Otherwise, null will be returned.
     */
    public DocumentViewWidget otherChild(DocumentViewWidget child) {
        if (child == first) {
            return second;
        } else if (child == second) {
            return first;
        } else {
            return null;
        }
    }

    /**
     * Returns the position of the first child
     */
    public Position getFirstChildPosition() {
        return new Position(0, 0);
    }

    /**
     * Returns the position of the second child
     */
    public Position getSecondChildPosition() {
        int separator = getSeparatorOffset();

        if (this.orientation == Orientation.Vertical) {
            return new Position(0, separator + SEPARATOR_WIDTH / 2);
        } else {
            return new Position(separator + SEPARATOR_WIDTH / 2, 0);
        }
    }

    /**
     * Draws a separator between the children
     */
    private void drawSeparator(Graphics g) {
        int separator = getSeparatorOffset();

        if (this.orientation == Orientation.Vertical) {
            g.drawLine(0, separator, getSize().getWidth(), separator);
        } else {
            g.drawLine(separator, 0, separator, getSize().getHeight());
        }
    }

    /**
     * Sets the child size variables of this split
     */
    private void setChildSizes(Size maxSize) {
        int separator = getSeparatorOffset();

        firstSize = maxSize;
        secondSize = maxSize;

        if (this.orientation == Orientation.Vertical) {
            firstSize = firstSize.setHeight(separator - SEPARATOR_WIDTH / 2);
            secondSize = secondSize.setHeight((maxSize.getHeight() - separator) - SEPARATOR_WIDTH / 2);
        } else {
            firstSize = firstSize.setWidth(separator - SEPARATOR_WIDTH / 2);
            secondSize = secondSize.setWidth((maxSize.getWidth() - separator) - SEPARATOR_WIDTH / 2);
        }
    }

    /**
     * Checks whether the given position is on the separator
     */
    private boolean isPositionOnSeparator(Position pos) {
        int separator = getSeparatorOffset();

        if (this.orientation == Orientation.Vertical) {
            return pos.getY() > separator - SEPARATOR_WIDTH / 2 && pos.getY() < separator + SEPARATOR_WIDTH / 2;
        } else {
            return pos.getX() > separator - SEPARATOR_WIDTH / 2 && pos.getX() < separator + SEPARATOR_WIDTH / 2;
        }
    }

    private int clampSeparatorOffset(int offset) {
        if (this.orientation == Orientation.Vertical) {
            return Math.min(getSize().getHeight() - SEPARATOR_WIDTH / 2, Math.max(offset, SEPARATOR_WIDTH / 2));
        } else {
            return Math.min(getSize().getWidth() - SEPARATOR_WIDTH / 2, Math.max(offset, SEPARATOR_WIDTH / 2));
        }
    }

    /**
     * Returns the offset of the separator from the origin based on the current value of separatorFraction
     * <p>
     * If the direction is Vertical, the offset will be a y-coordinate, otherwise
     * if the direction is Horizontal, the offset will be a x-coordinate.
     */
    public int getSeparatorOffset() {
        if (this.orientation == Orientation.Vertical) {
            return clampSeparatorOffset((int) (separatorFraction * getSize().getHeight()));
        } else {
            return clampSeparatorOffset((int) (separatorFraction * getSize().getWidth()));
        }
    }

    @Override
    public void receiveFocus() {
        first.receiveFocus();
    }

    @Override
    public void render(Graphics g) {
        super.render(g);

        ((Widget) first).render(GraphicsUtil.clone(g, getFirstChildPosition(), firstSize));
        ((Widget) second).render(GraphicsUtil.clone(g, getSecondChildPosition(), secondSize));
        drawSeparator(g);
    }

    @Override
    public void layout(Size maxSize) {
        setSize(maxSize);
        setChildSizes(maxSize);

        ((Widget) first).layout(firstSize);
        ((Widget) second).layout(secondSize);
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);

        ((Widget) first).handleMouseEvent(e.translate(getFirstChildPosition()));
        ((Widget) second).handleMouseEvent(e.translate(getSecondChildPosition()));

        if (e.isWithin(this.getSize()) && e.getType() == MouseEvent.Type.DRAGGED) {
            if (isPositionOnSeparator(e.getPosition())) {
                if (orientation == Orientation.Vertical) {
                    separatorFraction = ((double) e.getPosition().getY()) / ((double) getSize().getHeight());
                } else {
                    separatorFraction = ((double) e.getPosition().getX()) / ((double) getSize().getWidth());
                }

                getContext().requestLayout();
                getContext().requestRepaint();
            }
        }
    }

    @Override
    public void handleCloseFrame(Frame closed) {
        documentViewHandler.handleCollapseSplit(this, otherChild(closed));
    }

    @Override
    public void handleSplitFrame(Frame splitted, Orientation or) {
        Frame newFrame = new Frame(getContext());
        newFrame.registerUrlChangedHandler(splitted.getUrlChangedHandler());
        newFrame.registerSwitchFrameHandler(splitted.getSwitchFrameHandler());

        Split newSplit = new Split(getContext(), or, splitted, newFrame);
        newSplit.registerDocumentViewHandler(this);

        splitted.registerDocumentViewHandler(newSplit);
        newFrame.registerDocumentViewHandler(newSplit);

        if (splitted == first) {
            first = newSplit;
        } else {
            second = newSplit;
        }

        newFrame.loadDocument(splitted.getUrl());
    }

    @Override
    public void handleCollapseSplit(Split collapsed, DocumentViewWidget child) {
        if (collapsed == first) {
            first = child;
        } else {
            second = child;
        }
        child.registerDocumentViewHandler(this);
        child.receiveFocus();
        getContext().requestLayout();
        getContext().requestRepaint();
    }
}
