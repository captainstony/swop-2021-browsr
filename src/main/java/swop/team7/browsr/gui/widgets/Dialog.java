package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Dialog is a Widget that draws a dialog with a title, a content section and a sequence of buttons.
 */
public class Dialog extends Container {
    private Color backgroundColor;
    private Color borderColor;

    /**
     * Creates a new Dialog widget in the given context with the given title, content, buttons and the given colors.
     * <p>
     * The size of the dialog will be constrained to the given size.
     */
    public Dialog(WidgetContext widgetContext, String title, Widget content, ArrayList<Button> buttons, Size size, Color backgroundColor, Color borderColor) {
        super(widgetContext);
        this.backgroundColor = backgroundColor;
        this.borderColor = borderColor;

        Widget buttonsLayout = new LinearLayout(
                getContext(),
                buttons.stream().map(b -> (Widget) b).collect(Collectors.toList()),
                Orientation.Horizontal,
                10
        );

        Widget layout = new LinearLayout(getContext(), Orientation.Vertical)
                .with(content)
                .with(new FloatContainer(
                        getContext(), buttonsLayout,
                        true, true,
                        FloatContainer.HorizontalFloatPosition.Right,
                        FloatContainer.VerticalFloatPosition.Bottom
                ));

        Widget innerBox = new Box(getContext(), layout, 10);

        Widget topLayout = new LinearLayout(getContext(), Orientation.Vertical)
                .with(new Box(getContext(), new FloatContainer(
                        getContext(),
                        new Text(getContext(), title, getContext().getDefaultFont(14), Color.BLACK),
                        true, false,
                        FloatContainer.HorizontalFloatPosition.Center,
                        FloatContainer.VerticalFloatPosition.Center
                ), 0, 0, Color.BLACK, borderColor))
                .with(new FloatContainer(
                        getContext(), innerBox,
                        true, true,
                        FloatContainer.HorizontalFloatPosition.Center,
                        FloatContainer.VerticalFloatPosition.Center
                ));

        Widget box = new FloatContainer(getContext(),
                new ConstrainedContainer(getContext(),
                        new Box(getContext(), topLayout, 5, 5,
                                borderColor,
                                backgroundColor
                        ),
                        size
                ), true, true,
                FloatContainer.HorizontalFloatPosition.Center,
                FloatContainer.VerticalFloatPosition.Center
        );

        this.setChild(box);
    }

    /**
     * Creates a new Dialog widget in the given context with the given title, content, buttons and the default colors.
     * <p>
     * The size of the dialog will be constrained to the given size.
     */
    public Dialog(WidgetContext widgetContext, String title, Widget content, ArrayList<Button> buttons, Size size) {
        this(widgetContext, title, content, buttons, size, new Color(105, 173, 232), new Color(10, 91, 149));
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }
}
