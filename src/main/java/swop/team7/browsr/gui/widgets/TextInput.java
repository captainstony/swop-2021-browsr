package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.focus.KeyboardFocusManager;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.handlers.CancelTextEditHandler;
import swop.team7.browsr.handlers.ConfirmTextEditHandler;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

/**
 * Text is a Widget that displays text in a given font and color.
 */
public class TextInput extends Widget {
    public static int MARGIN = 5;
    private final int margin = 5;
    private String text;
    private Font font;
    private Color color;
    private int beginHighlight = 0;
    private int endHighlight = 0;
    private int cursorPosition = 0;
    private boolean highlight = false;

    private CancelTextEditHandler cancelTextEditHandler;
    private ConfirmTextEditHandler confirmTextEditHandler;

    /**
     * Create a new Text widget in the given context with the given text, font and color.
     */
    public TextInput(WidgetContext context, String text, Font font, Color color) {
        super(context);
        this.text = text;
        this.font = font;
        this.color = color;
    }

    /**
     * Create a new Text widget in the given context with the given text.
     * It will use the default font provided by the context and use a black color.
     */
    public TextInput(WidgetContext context, String text) {
        this(context, text, context.getDefaultFont(), Color.BLACK);
    }

    /**
     * Create a new Text widget in the given context with no text.
     * It will use the default font provided by the context and use a black color.
     */
    public TextInput(WidgetContext context) {
        this(context, "", context.getDefaultFont(), Color.BLACK);
    }

    public void registerCancelTextEditHandler(CancelTextEditHandler handler) {
        this.cancelTextEditHandler = handler;
    }

    public void registerConfirmTextEditHandler(ConfirmTextEditHandler handler) {
        this.confirmTextEditHandler = handler;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void render(Graphics g) {
        super.render(g);
        Font font = getContext().getDefaultFont();
        g.setFont(font);
        g.setColor(Color.BLACK);
        if (highlight) {
            g.setColor(Color.decode("#3c8ffc"));
            g.fillRect(MARGIN + getContext().getTextSize(font, getText().substring(0, beginHighlight)).getWidth(),
                    MARGIN,
                    getContext().getTextSize(font, getText().substring(beginHighlight, endHighlight)).getWidth(),
                    getContext().getTextHeight(font));
            g.setColor(Color.BLACK);
        }
        g.setFont(getContext().getDefaultFont());
        if (KeyboardFocusManager.isFocussed(this)) {
            g.drawString("|",
                    MARGIN + getContext().getTextSize(font, getText().substring(0, cursorPosition)).getWidth()
                            - getContext().getTextSize(font, "|").getWidth() / 2,
                    MARGIN + getContext().getTextOffset(font));
        }
        g.setFont(font);
        g.setColor(color);
        g.drawString(text, margin, this.getContext().getTextOffset(font) + margin);
    }

    @Override
    public void layout(Size maxSize) {
        this.setSize(getContext().getTextSize(font, this.text).add(new Size(2 * margin, 2 * margin)));
    }

    /**
     * Set whether a portion of text of the TextInputWidget is highlighted.
     *
     * @param b The boolean value indicating whether a portion of text of the TextInputWidget is highlighted.
     */
    public void setHighlight(Boolean b) {
        highlight = b;
    }

    /**
     * Get the position of the cursor of the AddressBar
     *
     * @return Integer value representing the position of the cursor of the TextInputWidget
     * You may assume that this value is in the range of [0, text.length()]
     */
    public int getCursorPosition() {
        return cursorPosition;
    }

    /**
     * Check whether a portion of text of the TextInputWidget is highlighted
     *
     * @return Boolean indicating whether a portion of text of the TextInputWidget is highlighted
     */
    public boolean isHighlighted() {
        return highlight;
    }

    /**
     * Get the range of
     *
     * @return A list of integer values containing the cursor positions where the highlighted text begins as its first element
     * and the cursor position where the highlighted text ends as its second and final element
     * You may assume that both values are in the range of [0, text.length()]
     * You may assume that the first element is strictly less than the second element
     */
    public List<Integer> getHighlightedRange() {
        return Arrays.asList(beginHighlight, endHighlight);
    }

    /**
     * Handle the event where this TextInputWidget was clicked on while it had not received focus.
     */
    public void handleClickedIn() {
        if (KeyboardFocusManager.isFocussed(this))
            return;

        if (KeyboardFocusManager.requestFocus(this)) {
            cursorPosition = getText().length();
            highlight = true;
            beginHighlight = 0;
            endHighlight = getText().length();
            getContext().requestRepaint();
        }
    }

    /**
     * Handle the event where there was clicked outside of this TextInputWidget while it had received focus.
     */
    public void handleClickedOut() {
        if (!KeyboardFocusManager.isFocussed(this))
            return;

        if (this.confirmTextEditHandler != null) {
            this.confirmTextEditHandler.handleConfirmTextEdit();
        }
        releaseFocus();
    }

    @Override
    public void handleKeyEvent(KeyEvent e) {
        if (!KeyboardFocusManager.isFocussed(this)) {
            return;
        }

        if (e.getType() == KeyEvent.Type.PRESSED) {
            switch (e.getCode()) {
                case KeyEvent.BACKSPACE:
                    handleBackspace();
                    return;

                case KeyEvent.DELETE:
                    handleDelete();
                    return;

                case KeyEvent.LEFT:
                    handleLeft(e.getModifier());
                    return;

                case KeyEvent.RIGHT:
                    handleRight(e.getModifier());
                    return;

                case KeyEvent.HOME:
                    handleHome(e.getModifier());
                    return;

                case KeyEvent.END:
                    handleEnd(e.getModifier());
                    return;

                case KeyEvent.ENTER:
                    handleEnter();
                    return;

                case KeyEvent.ESCAPE:
                    handleEscape();
                    return;

                default:
                    if (Character.isDefined(e.getCharacter())) {
                        if (e.getModifier() == Modifier.CTRL) {
                            return;
                        }
                        if (highlight) {
                            setText(getText().substring(0, beginHighlight) + getText().substring(endHighlight));
                            cursorPosition = beginHighlight;
                            highlight = false;
                        }
                        setText(getText().substring(0, cursorPosition) + e.getCharacter() + getText().substring(cursorPosition));
                        cursorPosition = cursorPosition + 1;
                        getContext().requestLayout();
                        getContext().requestRepaint();
                    }
            }
        }
    }

    /**
     * Handle the event where the backspace key was pressed while this TextInputWidget had received focus.
     */
    private void handleBackspace() {
        if (highlight) {
            setText(getText().substring(0, beginHighlight) + getText().substring(endHighlight));
            cursorPosition = beginHighlight;
            highlight = false;
        } else {
            setText(getText().substring(0, Math.max(cursorPosition - 1, 0)) + getText().substring(cursorPosition));
            cursorPosition = Math.max(cursorPosition - 1, 0);
        }
        getContext().requestLayout();
        getContext().requestRepaint();
    }

    /**
     * Handle the event where the delete key was pressed while this TextInputWidget had received focus.
     */
    private void handleDelete() {
        if (highlight) {
            setText(getText().substring(0, beginHighlight) + getText().substring(endHighlight));
            cursorPosition = beginHighlight;
            highlight = false;
        } else {
            setText(getText().substring(0, cursorPosition) + getText().substring(Math.min(cursorPosition + 1, getText().length())));
        }
        getContext().requestLayout();
        getContext().requestRepaint();
    }

    /**
     * Handle the event where the left key was pressed while this TextInputWidget had received focus.
     *
     * @param mod The modifier for the left key press.
     */
    private void handleLeft(Modifier mod) {
        if (highlight) {
            if (mod == Modifier.SHIFT) {
                if (cursorPosition == beginHighlight) {
                    beginHighlight = Math.max(beginHighlight - 1, 0);
                } else {
                    if (endHighlight > beginHighlight + 1) {
                        endHighlight = endHighlight - 1;
                    } else {
                        highlight = false;
                    }
                }
            } else {
                highlight = false;
            }
        } else {
            if (mod == Modifier.SHIFT && cursorPosition > 0) {
                highlight = true;
                beginHighlight = cursorPosition - 1;
                endHighlight = cursorPosition;
            }
        }
        cursorPosition = Math.max(cursorPosition - 1, 0);
        getContext().requestRepaint();
    }

    /**
     * Handle the event where the right key was pressed while this TextInputWidget had received focus.
     *
     * @param mod The modifier for the right key press.
     */
    private void handleRight(Modifier mod) {
        if (highlight) {
            if (mod == Modifier.SHIFT) {
                if (cursorPosition == beginHighlight) {
                    if (beginHighlight < endHighlight - 1) {
                        beginHighlight = beginHighlight + 1;
                    } else {
                        highlight = false;
                    }
                } else {
                    endHighlight = Math.min(endHighlight + 1, getText().length());
                }
            } else {
                highlight = false;
            }
        } else {
            if (mod == Modifier.SHIFT && cursorPosition < getText().length()) {
                highlight = true;
                beginHighlight = cursorPosition;
                endHighlight = cursorPosition + 1;
            }
        }
        cursorPosition = Math.min(cursorPosition + 1, getText().length());
        getContext().requestRepaint();
    }

    /**
     * Handle the event where the home key was pressed while this TextInputWidget had received focus.
     *
     * @param mod The modifier for the home key press.
     */
    private void handleHome(Modifier mod) {
        if (highlight) {
            if (mod == Modifier.SHIFT && beginHighlight > 0) {
                endHighlight = beginHighlight;
                beginHighlight = 0;
            } else {
                highlight = false;
            }
        } else {
            if (mod == Modifier.SHIFT && cursorPosition > 0) {
                highlight = true;
                beginHighlight = 0;
                endHighlight = cursorPosition;
            }
        }
        cursorPosition = 0;
        getContext().requestRepaint();
    }

    /**
     * Handle the event where the end key was pressed while this TextInputWidget had received focus.
     *
     * @param mod The modifier for the end key press.
     */
    private void handleEnd(Modifier mod) {
        if (highlight) {
            if (mod == Modifier.SHIFT && endHighlight < getText().length()) {
                beginHighlight = endHighlight;
                endHighlight = getText().length();
            } else {
                highlight = false;
            }
        } else {
            if (mod == Modifier.SHIFT && cursorPosition < getText().length()) {
                highlight = true;
                beginHighlight = cursorPosition;
                endHighlight = getText().length();
            }
        }
        cursorPosition = getText().length();
        getContext().requestRepaint();
    }

    private void releaseFocus() {
        KeyboardFocusManager.releaseFocussed(this);
        this.setHighlight(false);
        getContext().requestRepaint();
    }

    /**
     * Handle the event where the enter key was pressed while this TextInputWidget had received focus.
     */
    private void handleEnter() {
        if (this.confirmTextEditHandler != null) {
            this.confirmTextEditHandler.handleConfirmTextEdit();
        }
        releaseFocus();
    }

    /**
     * Handle the event where the escape key was pressed while this TextInputWidget had received focus.
     */
    private void handleEscape() {
        if (this.cancelTextEditHandler != null) {
            this.cancelTextEditHandler.handleCancelTextEdit();
        }
        releaseFocus();
    }
}
