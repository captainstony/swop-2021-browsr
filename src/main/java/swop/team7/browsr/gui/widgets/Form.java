package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.handlers.ButtonClickHandler;
import swop.team7.browsr.handlers.FormSubmitHandler;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Form is a Widget that represents a form with input fields and a submission action.
 * <p>
 * It has a single child widget and stores a list of {@link InputField}.
 * The action field is used to describe the action when submitting this form.
 * <p>
 * Forms also implements a {@link ButtonClickHandler}. This is used to respond to
 * clicking a submit {@link Button}. A submit button widget should exist and should have this form
 * as its {@link ButtonClickHandler}.
 * <p>
 * {@link InputField} widgets can be registered with {@link #registerTextInputField(InputField)}.
 * A {@link FormSubmitHandler} can be registered with {@link #registerFormSubmitHandler(FormSubmitHandler)}.
 */
public class Form extends Widget implements ButtonClickHandler {
    private final Map<String, InputField> textInputFields = new HashMap<>();
    private final String action;
    private Widget child;
    private FormSubmitHandler handler;

    /**
     * Creates a new Form in the given context with the given action and child.
     */
    public Form(WidgetContext context, String action, Widget child) {
        super(context);
        this.child = child;
        this.action = action;
    }

    public Widget getChild() {
        return child;
    }

    public void setChild(Widget child) {
        this.child = child;
    }

    @Override
    public void render(Graphics g) {
        super.render(g);
        child.render(g);
    }

    @Override
    public void layout(Size maxSize) {
        this.child.layout(maxSize);
        this.setSize(this.child.getSize());
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);
        child.handleMouseEvent(e);
    }

    /**
     * This method will invoke the {@link FormSubmitHandler} of this {@link Form}.
     */
    @Override
    public void handleClick() {
        Map<String, String> textInputs = new HashMap<>();
        for (Map.Entry<String, InputField> entry : textInputFields.entrySet()) {
            textInputs.put(entry.getKey(), entry.getValue().getText());
        }
        handler.handleFormSubmit(action, textInputs);
    }

    public void registerTextInputField(InputField inputField) {
        textInputFields.put(inputField.getName(), inputField);
    }

    public void registerFormSubmitHandler(FormSubmitHandler handler) {
        this.handler = handler;
    }
}
