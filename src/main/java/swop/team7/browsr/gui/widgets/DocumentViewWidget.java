package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.handlers.DocumentViewHandler;

/**
 * Interface representing widgets that are a part of the document view structure.
 * Objects implementing this interface should always (directly or indirectly) extend the abstract Widget class.
 */
public interface DocumentViewWidget {
    void receiveFocus();

    void registerDocumentViewHandler(DocumentViewHandler documentViewHandler);
}
