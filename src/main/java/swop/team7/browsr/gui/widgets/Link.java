package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.handlers.UrlChangedHandler;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.util.HashMap;

/**
 * Link is a type of Text Widget that represents hyperlinks.
 * <p>
 * It contains two fields, a target and a text field.
 * A {@link UrlChangedHandler} can be registered to respond to click events.
 */
public class Link extends Text {
    private final String target;
    private final String text;
    private UrlChangedHandler handler;

    public Link(WidgetContext context, String text, String target) {
        super(context, text, context.getDefaultFont(12)
                .deriveFont(new HashMap<TextAttribute, Integer>() {{
                    put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                }}), Color.BLUE);
        this.target = target;
        this.text = text;
    }

    public String getTarget() {
        return target;
    }

    public String getText() {
        return text;
    }

    public void registerUrlChangedHandler(UrlChangedHandler handler) {
        this.handler = handler;
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);
        if (e.getType() == MouseEvent.Type.CLICKED && e.isWithin(this.getSize())) {
            if (this.handler != null)
                this.handler.handleUrlChanged(this.target);
        }
    }
}
