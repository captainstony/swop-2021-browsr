package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.*;
import swop.team7.browsr.handlers.ScrollbarHandler;

import java.awt.*;

/**
 * ScrollableContainer is a Container widget that allows for scrolling within the view it provides.
 */
public class ScrollableContainer extends Container implements ScrollbarHandler {
    private int xScroll = 0;
    private int yScroll = 0;
    private ScrollBar horizontalScrollBar;
    private ScrollBar verticalScrollBar;

    /**
     * Create a new ScrollableContainer that contains the given widget in its view.
     */
    public ScrollableContainer(WidgetContext context, Widget child) {
        super(context, child);
    }

    /**
     * Create a new empty ScrollableContainer.
     */
    public ScrollableContainer(WidgetContext context) {
        super(context);
    }

    /**
     * Register that a scroll bar for the given direction is to be included in the scrollable container.
     *
     * @param or The orientation for the scroll bar that is registered.
     */
    public void registerScrollBar(Orientation or) {
        if (or == Orientation.Horizontal) {
            horizontalScrollBar = new ScrollBar(getContext(), Orientation.Horizontal);
            horizontalScrollBar.registerScrollBarHandler(this);
        } else {
            verticalScrollBar = new ScrollBar(getContext(), Orientation.Vertical);
            verticalScrollBar.registerScrollBarHandler(this);
        }
    }

    /**
     * Get the size of the view of this scrollable container.
     *
     * @return The size of the view of this container.
     */
    public Size getViewSize() {
        Size viewSize = new Size(getSize().getWidth(), getSize().getHeight());
        if (horizontalScrollBar != null) {
            viewSize = viewSize.add(new Size(0, -ScrollBar.barThickness));
        }
        if (verticalScrollBar != null) {
            viewSize = viewSize.add(new Size(-ScrollBar.barThickness, 0));
        }

        return viewSize;
    }

    @Override
    public void layout(Size maxSize) {
        this.setSize(maxSize);

        Size viewSize = getViewSize();

        getChild().layout(viewSize);

        if (horizontalScrollBar != null) {
            horizontalScrollBar.layout(viewSize);

            double horizontalFraction = (double) viewSize.getWidth() / (double) getChild().getSize().getWidth();
            this.horizontalScrollBar.setVisibleFraction(horizontalFraction);
        }
        if (verticalScrollBar != null) {
            verticalScrollBar.layout(viewSize);

            double verticalFraction = (double) viewSize.getHeight() / (double) getChild().getSize().getHeight();
            this.verticalScrollBar.setVisibleFraction(verticalFraction);
        }
    }

    @Override
    public void render(Graphics g) {
        Size viewSize = getViewSize();

        if (horizontalScrollBar != null) {
            Graphics gHorizontal = GraphicsUtil.clone(g, new Position(0, viewSize.getHeight()), horizontalScrollBar.getSize());
            horizontalScrollBar.render(gHorizontal);
        }
        if (verticalScrollBar != null) {
            Graphics gVertical = GraphicsUtil.clone(g, new Position(viewSize.getWidth(), 0), verticalScrollBar.getSize());
            verticalScrollBar.render(gVertical);
        }

        Graphics gChild = GraphicsUtil.clone(g, new Position(xScroll, yScroll), viewSize.add(new Size(-xScroll, -yScroll)));
        super.render(gChild);
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e.translate(xScroll, yScroll));

        Size viewSize = this.getViewSize();

        if (this.verticalScrollBar != null) {
            this.verticalScrollBar.handleMouseEvent(e.translate(new Position(viewSize.getWidth(), 0)));
        }
        if (this.horizontalScrollBar != null) {
            this.horizontalScrollBar.handleMouseEvent(e.translate(new Position(0, viewSize.getHeight())));
        }
    }

    @Override
    public void handleScroll(Orientation or, int units) {
        if (or == Orientation.Horizontal) {
            xScroll = units;
        } else {
            yScroll = units;
        }

        this.getContext().requestLayout();
        this.getContext().requestRepaint();
    }

    public ScrollBar getHorizontalScrollBar() {
        return horizontalScrollBar;
    }

    public ScrollBar getVerticalScrollBar() {
        return verticalScrollBar;
    }
}
