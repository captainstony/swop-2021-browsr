package swop.team7.browsr.gui.widgets;

import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.handlers.ScrollbarHandler;

import java.awt.*;

/**
 * ScrollBar is the widget representing either a horizontal or vertical scroll bar.
 */
public class ScrollBar extends Widget {
    public static int barThickness = 10;
    private final Orientation or;
    private int offset = 0;
    private int barLength;
    private int scrollerLength;
    private double visibleFraction;
    private ScrollbarHandler scrollbarHandler;

    /**
     * Create a new ScrollBar with the given orientation.
     */
    public ScrollBar(WidgetContext widgetContext, Orientation or) {
        super(widgetContext);

        this.or = or;
    }

    /**
     * Register the given ScrollBarHandler as ScrollBarHandler for this scroll bar.
     *
     * @param handler The ScrollBarHandler to be registered.
     */
    public void registerScrollBarHandler(ScrollbarHandler handler) {
        this.scrollbarHandler = handler;
    }

    /**
     * Set the visible fraction of the view that is associated with this scroll bar and
     * update the length of the sroller accordingly.
     *
     * @param visibleFraction The fraction of content that is visible in the view associated
     *                        with this scroll bar that is visible for its orientation.
     */
    public void setVisibleFraction(double visibleFraction) {
        this.visibleFraction = visibleFraction;
        this.scrollerLength = (int) (this.visibleFraction * this.barLength);
    }

    public int getOffset() {
        return offset;
    }

    /**
     * Set the offset of the scroller within the scroll bar for the given offset.
     * <p>
     * The offset of a scroll bar must always be between 0 and the total length of the scroll bar minus the length of the scroller.
     * If the given offset is negative, the offset of this scrolll bar is set to 0.
     * If the given offset exceeds the maximum, the offset of this scroll bar is set to its maximum value.
     *
     * @param offset The new offset for this scroll bar.
     */
    public void setOffset(int offset) {
        if (offset >= 0 && offset <= this.barLength - this.scrollerLength) {
            this.offset = offset;
        } else if (offset < 0) {
            this.offset = 0;
        } else if (offset >= this.barLength - this.scrollerLength) {
            this.offset = this.barLength - this.scrollerLength;
        }
    }

    @Override
    public void layout(Size maxSize) {
        if (or == Orientation.Horizontal) {
            setSize(maxSize.getWidth(), barThickness);
            this.barLength = this.getSize().getWidth();
        } else {
            setSize(barThickness, maxSize.getHeight());
            this.barLength = this.getSize().getHeight();
        }
    }

    @Override
    public void render(Graphics g) {
        super.render(g);

        if (or == Orientation.Horizontal) {
            // scrollBar
            g.setColor(Color.LIGHT_GRAY);
            g.fillRect(0, 0, getSize().getWidth(), getSize().getHeight());

            // scroller
            g.setColor(Color.DARK_GRAY);
            g.fillRect(this.offset, 0, this.scrollerLength, getSize().getHeight());
        } else {
            // scrollBar
            g.setColor(Color.LIGHT_GRAY);
            g.fillRect(0, 0, getSize().getWidth(), getSize().getHeight());

            // scroller
            g.setColor(Color.DARK_GRAY);
            g.fillRect(0, this.offset, getSize().getWidth(), this.scrollerLength);
        }

    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        super.handleMouseEvent(e);

        if (e.isWithin(this.getSize()) && e.getType() == MouseEvent.Type.DRAGGED) {
            if (or == Orientation.Horizontal) {
                this.setOffset(e.getPosition().getX() - this.scrollerLength / 2);
                this.scrollbarHandler.handleScroll(Orientation.Horizontal, (int) Math.floor(-this.offset / this.visibleFraction));
            } else {
                this.setOffset(e.getPosition().getY() - this.scrollerLength / 2);
                this.scrollbarHandler.handleScroll(Orientation.Vertical, (int) Math.floor(-this.offset / this.visibleFraction));
            }
            this.getContext().requestLayout();
            this.getContext().requestRepaint();
        }
    }

    public int getBarLength() {
        return this.barLength;
    }


    public int getScrollerLength() {
        return this.scrollerLength;
    }
}
