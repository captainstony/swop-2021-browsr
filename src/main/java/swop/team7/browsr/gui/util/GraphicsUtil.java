package swop.team7.browsr.gui.util;

import java.awt.*;

/**
 * Class with utility functions
 */
public class GraphicsUtil {
    /**
     * Creates a clone of the given Graphics object, translated to the given position and clipped to the given size.
     */
    public static Graphics clone(Graphics original, Position pos, Size size) {
        return original.create(pos.getX(), pos.getY(), size.getWidth(), size.getHeight());
    }
}
