package swop.team7.browsr.gui.util;

import java.awt.*;
import java.util.Objects;

/**
 * A Size object containing a width and height
 */
public class Size {
    public static final Size ZERO = new Size(0, 0);
    private final int width;
    private final int height;

    /**
     * Creates a new Position from the given dimensions
     */
    public Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public Size setHeight(int height) {
        return new Size(this.width, height);
    }

    public int getWidth() {
        return width;
    }

    public Size setWidth(int width) {
        return new Size(width, this.height);
    }

    /**
     * Converts the size to a Rectangle with this size and the given position as origin
     */
    public Rectangle toRectangle(Position position) {
        return new Rectangle(position.getX(), position.getY(), width, height);
    }

    /**
     * Adds two sizes together.
     * The width of the resulting Size is the sum of the width of the current Size and the given Size
     * The height of the resulting Size is the sum of the height of the current Size and the given Size
     */
    public Size add(Size size) {
        return new Size(this.getWidth() + size.getWidth(), this.getHeight() + size.getHeight());
    }

    /**
     * Creates a new Size constrained to the given size.
     * The dimensions of the returned size are equal to the minimum of this size and the given size.
     */
    public Size constrain(Size size) {
        return new Size(
                Math.min(getWidth(), size.getWidth()),
                Math.min(getHeight(), size.getHeight())
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Size size = (Size) o;
        return getWidth() == size.getWidth() && getHeight() == size.getHeight();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getWidth(), getHeight());
    }

    @Override
    public String toString() {
        return "Size{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
