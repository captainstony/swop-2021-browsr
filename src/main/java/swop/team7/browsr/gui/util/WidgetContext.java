package swop.team7.browsr.gui.util;

import java.awt.*;

public interface WidgetContext {
    /**
     * Returns the default font
     */
    Font getDefaultFont();

    /**
     * Returns the default font with the given size
     */
    Font getDefaultFont(int size);

    /**
     * Returns the height of the font
     */
    int getTextHeight(Font font);

    /**
     * Returns the Size required to display the given text in the font of this Style
     */
    Size getTextSize(Font font, String text);

    /**
     * Returns the offset of the font of this Style
     */
    int getTextOffset(Font font);

    /**
     * Request a layout of the current Widget Context
     */
    void requestLayout();

    /**
     * Request a redraw of the current Widget Context
     */
    void requestRepaint();
}
