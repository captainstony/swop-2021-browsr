package swop.team7.browsr.gui.util;

import java.awt.*;
import java.util.Objects;

/**
 * A Position object containing x and y coordinates
 */
public class Position {
    public static Position ZERO = new Position(0, 0);
    private final int x;
    private final int y;

    /**
     * Creates a new Position from the given coordinates
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Converts the position to a Rectangle with the given size and this position as origin
     */
    public Rectangle toRectangle(Size size) {
        return new Rectangle(x, y, size.getWidth(), size.getHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return getX() == position.getX() && getY() == position.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    @Override
    public String toString() {
        return "Position{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
