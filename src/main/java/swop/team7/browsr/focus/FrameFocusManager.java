package swop.team7.browsr.focus;

import swop.team7.browsr.gui.widgets.Frame;

/**
 * FrameFocusManager manages the currently focussed Frame globally.
 */
public class FrameFocusManager {
    private static Frame focussed;

    private FrameFocusManager() {
    }

    /**
     * Get the currently focussed frame.
     *
     * @return The currently focussed frame.
     */
    public static Frame getFocussed() {
        return focussed;
    }

    /**
     * If able to give focus to frame:
     * give focus and return true
     *
     * @return true if frame has been given focus
     */
    public static boolean requestFocus(Frame frame) {
        focussed = frame;
        return true;
    }

    /**
     * Returns true if the frame is currently focussed
     *
     * @return true if frame is focussed
     */
    public static boolean isFocussed(Frame frame) {
        return focussed == frame;
    }

    /**
     * Sets focus to null if the frame asking for release
     * is the currently focussed frame.
     */
    public static void releaseFocussed(Frame frame) {
        if (focussed == frame) {
            focussed = null;
        }
    }
}
