package swop.team7.browsr.focus;

import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.widgets.Widget;

/**
 * KeyBoardFocusManager manages the currently focussed widget for key strokes globally.
 */
public class KeyboardFocusManager {
    private static Widget focussed;

    private KeyboardFocusManager() {
    }

    /**
     * If able to give focus to widget:
     * give focus and return true
     *
     * @return true if widget has been given focus
     */
    public static boolean requestFocus(Widget widget) {
        focussed = widget;
        return true;
    }

    /**
     * Returns true if the widget is currently focussed
     *
     * @return true if widget is focussed
     */
    public static boolean isFocussed(Widget widget) {
        return focussed == widget;
    }

    /**
     * Sets focus to null if the widget asking for release
     * is the currently focussed widget.
     */
    public static void releaseFocussed(Widget widget) {
        if (focussed == widget) {
            focussed = null;
        }
    }

    /**
     * Sends key events to the currently focussed widget
     */
    public static void sendKeyEventToFocussed(KeyEvent event) {
        if (focussed != null) {
            focussed.handleKeyEvent(event);
        }
    }

    /**
     * Sets the current focus so that no widget is focussed
     */
    public static void clearFocus() {
        focussed = null;
    }
}
