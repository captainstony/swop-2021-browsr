package swop.team7.browsr.handlers;

public interface ConfirmTextEditHandler {
    void handleConfirmTextEdit();
}
