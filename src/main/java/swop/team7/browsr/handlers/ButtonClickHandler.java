package swop.team7.browsr.handlers;

public interface ButtonClickHandler {
    void handleClick();
}
