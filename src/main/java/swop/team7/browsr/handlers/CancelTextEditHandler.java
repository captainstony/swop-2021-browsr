package swop.team7.browsr.handlers;

public interface CancelTextEditHandler {
    void handleCancelTextEdit();
}
