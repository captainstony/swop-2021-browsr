package swop.team7.browsr.handlers;

import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.widgets.DocumentViewWidget;
import swop.team7.browsr.gui.widgets.Frame;
import swop.team7.browsr.gui.widgets.Split;

public interface DocumentViewHandler {
    void handleCloseFrame(Frame self);

    void handleSplitFrame(Frame self, Orientation or);

    void handleCollapseSplit(Split self, DocumentViewWidget child);
}
