package swop.team7.browsr.handlers;

import swop.team7.browsr.gui.widgets.Link;

public interface AddBookmarkHandler {
    void handleAddBookmark(Link bookmark);
}
