package swop.team7.browsr.handlers;

public interface ShowAddBookmarkDialogHandler {
    void showAddBookmarkDialog();
}
