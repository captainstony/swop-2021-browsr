package swop.team7.browsr.handlers;

public interface ShowSaveDialogHandler {
    void showSaveDialog();
}
