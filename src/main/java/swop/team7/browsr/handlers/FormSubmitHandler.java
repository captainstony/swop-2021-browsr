package swop.team7.browsr.handlers;

import java.util.Map;

public interface FormSubmitHandler {
    void handleFormSubmit(String action, Map<String, String> args);
}
