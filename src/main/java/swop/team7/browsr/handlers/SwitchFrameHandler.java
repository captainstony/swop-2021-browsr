package swop.team7.browsr.handlers;

public interface SwitchFrameHandler {
    void handleSwitchFrame();
}
