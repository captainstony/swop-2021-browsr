package swop.team7.browsr.handlers;

import swop.team7.browsr.gui.util.Orientation;

public interface ScrollbarHandler {
    void handleScroll(Orientation or, int units);
}
