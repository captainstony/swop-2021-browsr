package swop.team7.browsr.handlers;

public interface UrlChangedHandler {
    void handleUrlChanged(String url);
}
