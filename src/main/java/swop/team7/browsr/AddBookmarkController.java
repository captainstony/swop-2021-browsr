package swop.team7.browsr;

import swop.team7.browsr.gui.modals.AddBookmarkModal;
import swop.team7.browsr.gui.widgets.Link;
import swop.team7.browsr.handlers.AddBookmarkHandler;

/**
 * AddBookmarkController is the controller instance which operates for the modal of the dialog screen used to add bookmarks.
 */
public class AddBookmarkController extends Controller {
    private final String url;
    private AddBookmarkModal modal;
    private AddBookmarkHandler addBookmarkHandler;

    protected AddBookmarkController(Window window, Context context, String url) {
        super(window, context);
        this.url = url;
    }

    /**
     * Register the given handler AddBookmarkHandler as the AddBookmarkHandler for this controller.
     *
     * @param addBookmarkHandler The handler to be registered.
     */
    public void registerAddBookmarkHandler(AddBookmarkHandler addBookmarkHandler) {
        this.addBookmarkHandler = addBookmarkHandler;
    }

    /**
     * Get the modal for which this controller operates.
     *
     * @return The modal for which this controller operates.
     */
    public AddBookmarkModal getModal() {
        return modal;
    }

    /**
     * Show the modal for which this controller operates in the window.
     */
    public void showModal() {
        modal = new AddBookmarkModal(getContext(), url);
        modal.getSubmitButton().registerClickHandler(this::handleSubmit);
        modal.getCancelButton().registerClickHandler(this::handleCancel);
        window.showModal(modal);
    }

    /**
     * Handle the event where a new bookmark is submitted.
     */
    public void handleSubmit() {
        if (modal.getNameInput().getText().isEmpty()) {
            showErrorModal("Error: Bookmark name must be specified");
            return;
        }

        if (modal.getNameInput().getText().isEmpty()) {
            showErrorModal("Error: Bookmark URL must be specified");
            return;
        }

        addBookmarkHandler.handleAddBookmark(
                new Link(getContext(), modal.getNameInput().getText(), modal.getUrlInput().getText())
        );
        window.closeModal();
    }

    /**
     * Handle the event where the adding of a new bookmark is canceled.
     */
    public void handleCancel() {
        this.window.closeModal();
    }
}
