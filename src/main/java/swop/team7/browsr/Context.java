package swop.team7.browsr;

import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

public class Context implements WidgetContext {
    private final Window window;

    public Context(Window window) {
        this.window = window;
    }

    @Override
    public Font getDefaultFont() {
        return getDefaultFont(12);
    }

    @Override
    public Font getDefaultFont(int size) {
        return new Font(Font.DIALOG, Font.PLAIN, size);
    }

    /**
     * Returns the offset of the font of this Style
     */
    @Override
    public int getTextOffset(Font font) {
        return window.getFontMetrics(font).getAscent();
    }

    /**
     * Returns the height of the font
     */
    @Override
    public int getTextHeight(Font font) {
        return window.getFontMetrics(font).getHeight();
    }

    /**
     * Returns the Size required to display the given text in the font of this Style
     */
    @Override
    public Size getTextSize(Font font, String text) {
        return new Size(window.getFontMetrics(font).stringWidth(text), getTextHeight(font));
    }

    @Override
    public void requestLayout() {
        this.window.requestLayout();
    }

    @Override
    public void requestRepaint() {
        this.window.requestRedraw();
    }
}
