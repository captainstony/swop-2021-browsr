package swop.team7.browsr;

import swop.team7.browsr.gui.modals.ErrorModal;

/**
 * Controller is the base class for all controllers that operate in Browsr.
 * <p>
 * Controllers implement handlers for executing events called by other objects.
 */
public abstract class Controller {
    protected final Window window;
    protected final Context context;

    protected Controller(Window window, Context context) {
        this.window = window;
        this.context = context;
    }

    /**
     * Get the context in which this controller operates.
     *
     * @return The context associated with this controller.
     */
    public Context getContext() {
        return context;
    }

    /**
     * Get the window for which this controller operates.
     *
     * @return The window associated with this controller.
     */
    public Window getWindow() {
        return window;
    }

    /**
     * Show an error modal on the window displaying the given message.
     *
     * @param message The message to be displayed on the error modal.
     */
    protected void showErrorModal(String message) {
        ErrorModal errorModal = new ErrorModal(this.context, message);
        errorModal.getConfirmButton().registerClickHandler(window::closeModal);
        this.window.showModal(errorModal);
    }
}
