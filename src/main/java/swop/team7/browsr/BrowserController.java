package swop.team7.browsr;

import swop.team7.browsr.focus.FrameFocusManager;
import swop.team7.browsr.gui.modals.BrowserModal;
import swop.team7.browsr.gui.widgets.AddressBar;
import swop.team7.browsr.gui.widgets.BookmarkBar;
import swop.team7.browsr.gui.widgets.Frame;
import swop.team7.browsr.gui.widgets.Link;
import swop.team7.browsr.handlers.*;
import swop.team7.browsr.html.factory.WidgetFactory;
import swop.team7.browsr.html.parser.HtmlParser;
import swop.team7.browsr.html.parser.InvalidBrowsrDocumentException;
import swop.team7.browsr.html.parser.objects.HtmlObject;
import swop.team7.browsr.html.parser.objects.HtmlText;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * BrowserController is the controller instance which operates for the main browser modal.
 */
public class BrowserController extends Controller implements
        UrlChangedHandler,
        SwitchFrameHandler,
        ShowAddBookmarkDialogHandler,
        ShowSaveDialogHandler,
        AddBookmarkHandler,
        FormSubmitHandler,
        ShowWelcomePageHandler {
    private final String URL_DEFAULT = "Type url here";
    private BrowserModal modal;

    public BrowserController(Window window, Context context) {
        super(window, context);
    }

    public String getUrl() {
        Frame frame = FrameFocusManager.getFocussed();
        if (frame == null)
            return URL_DEFAULT;
        String url = frame.getUrl();
        if (url == null || url.startsWith("file://"))
            return URL_DEFAULT;
        return url;
    }

    /**
     * Get the modal for which this controller is the handler.
     *
     * @return The browser modal associated with this controller.
     */
    public BrowserModal getModal() {
        return modal;
    }

    /**
     * Return the address bar for which this controller operates.
     *
     * @return The address bar associated with this controller.
     */
    public AddressBar getAddressBar() {
        return this.modal.getAddressBar();
    }

    /**
     * Return the bookmark bar for which this controller operates.
     *
     * @return The bookmark bar associated with this controller.
     */
    public BookmarkBar getBookmarkBar() {
        return this.modal.getBookmarkBar();
    }

    /**
     * Get the page currently shown in the browser modal.
     *
     * @return A string representing the contents of the page currently shown.
     * @throws Exception No page is currently shown or the page shown is not derived from a valid url.
     */
    public String getCurrentPage() throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(getUrl()).openStream()));
        return reader.lines().collect(Collectors.joining());
    }

    /**
     * Read local file
     */
    private Reader getLocalResource(String resource) throws IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(resource);

        if (stream == null) {
            throw new IOException("Resource not found");
        }

        return new InputStreamReader(stream);
    }

    /**
     * Feeds htmlObject-tree to the WidgetFactory
     * Sets Widget-tree as child of htmlRoot
     *
     * @param htmlRoot Root element of htmlObject-Tree
     */
    private void setPage(HtmlObject htmlRoot, String url) {
        WidgetFactory factory = new WidgetFactory(this);
        FrameFocusManager.getFocussed().setHtmlRoot(factory.createWidget(htmlRoot));
        FrameFocusManager.getFocussed().setUrl(url);
    }

    /**
     * Parses the file string and puts it in a htmlObject-tree
     *
     * @param file to be parsed file string
     */
    public void setLocalPage(String file) {
        HtmlObject htmlRoot = null;

        try {
            htmlRoot = new HtmlParser(getLocalResource(file)).parse();
        } catch (InvalidBrowsrDocumentException | IOException e) {
            htmlRoot = new HtmlText("Unexpected Error: " + e);
        }

        this.setPage(htmlRoot, "file://" + file);
    }

    /**
     * Show the modal for which this controller operates in the window.
     */
    public void showModal() {
        modal = new BrowserModal(this.context);
        modal.registerShowAddBookMarkDialogHandler(this);
        modal.registerShowSaveDialogHandler(this);
        modal.registerShowWelcomePageHandler(this);
        modal.getAddressBar().registerUrlChangedHandler(this);

        // Load default links
        Link link1 = new Link(this.context, "Test Page", "https://people.cs.kuleuven.be/~bart.jacobs/browsrtest.html");
        Link link2 = new Link(this.context, "test", "test");
        // Load handlers for links
        link1.registerUrlChangedHandler(this);
        link2.registerUrlChangedHandler(this);
        // Add links to bookmarkBar
        this.modal.addBookmark(link1);
        this.modal.addBookmark(link2);

        showWelcomePage();
        this.getAddressBar().setUrl(this.getUrl());

        window.showModal(modal);
    }

    /**
     * Handles changing the url by loading the requested document.
     * <p>
     * If the document cannot be loaded, an appropriate error document will be loaded instead.
     * <p>
     * The address bar will be updated to reflect these changes and the window will be redrawn.
     */
    @Override
    public void handleUrlChanged(String url) {
        HtmlObject htmlRoot = null;
        String errorDocument = null;
        String urlString = url;

        if (url.startsWith("file://")) {
            errorDocument = url.replaceAll("^file://", "");
        } else {
            try {
                URL newUrl = new URL(url);
                try {
                    newUrl = new URL(new URL(getUrl()), url);
                } catch (MalformedURLException ignored) {
                }

                urlString = newUrl.toString();
                HtmlParser parser = new HtmlParser(newUrl);
                htmlRoot = parser.parse();
            } catch (InvalidBrowsrDocumentException e) {
                errorDocument = "html/invalid_document.html";
            } catch (MalformedURLException e) {
                errorDocument = "html/invalid_address.html";
            } catch (IOException e) {
                errorDocument = "html/not_found.html";
            }
        }

        if (htmlRoot == null) {
            setLocalPage(errorDocument);
        } else {
            this.setPage(htmlRoot, urlString);
        }

        this.modal.getAddressBar().setUrl(getUrl());

        this.window.requestLayout();
        this.window.requestRedraw();
    }

    /**
     * Create a new AddBookmarkController for the given URL string.
     *
     * @param urlString The URL string for the new AddBookmarkController.
     * @return The newly created AddBookmarkController.
     */
    public AddBookmarkController createAddBookmarkController(String urlString) {
        return new AddBookmarkController(this.window, this.context, urlString);
    }

    @Override
    public void showAddBookmarkDialog() {
        AddBookmarkController addBookmarkController = createAddBookmarkController(getUrl());
        addBookmarkController.registerAddBookmarkHandler(this);
        addBookmarkController.showModal();
    }

    /**
     * Create a new SaveController for the given page.
     *
     * @param page The page for the new SaveController.
     * @return The newly created SaveController.
     */
    public SaveController createSaveController(String page) {
        return new SaveController(this.window, this.context, page);
    }

    /**
     * Creates a new {@link SaveController} with the current page and calls {@link SaveController#showModal()}.
     * <p>
     * If getting the current page fails, an error modal will be shown instead.
     */
    @Override
    public void showSaveDialog() {
        SaveController saveController;
        try {
            saveController = createSaveController(this.getCurrentPage());
            saveController.showModal();
        } catch (Exception e) {
            showErrorModal("Error: Unable to save the current document");
        }
    }

    /**
     * Adds the given bookmark to the bookmark bar in the modal
     */
    @Override
    public void handleAddBookmark(Link bookmark) {
        bookmark.registerUrlChangedHandler(this);
        this.modal.addBookmark(bookmark);
    }

    /**
     * Changes the url to the current url with the given action and arguments encoded within the url.
     */
    @Override
    public void handleFormSubmit(String action, Map<String, String> args) {
        String newUrl = getUrl().substring(0, getUrl().lastIndexOf("/"));
        newUrl += "/" + action;
        boolean first = true;
        for (Map.Entry<String, String> entry : args.entrySet()) {
            if (first) {
                newUrl += "?" + entry.getKey() + "=" + entry.getValue();
            } else {
                newUrl += "&" + entry.getKey() + "=" + entry.getValue();
            }
            first = false;
        }

        handleUrlChanged(newUrl);
    }

    @Override
    public void showWelcomePage() {
        Frame welcomeFrame = new Frame(context);
        welcomeFrame.registerUrlChangedHandler(this);
        welcomeFrame.registerDocumentViewHandler(modal);
        welcomeFrame.registerSwitchFrameHandler(this);

        modal.setDocumentViewRoot(welcomeFrame);

        welcomeFrame.loadDocument("file://html/welcome.html");
    }

    @Override
    public void handleSwitchFrame() {
        this.modal.getAddressBar().setUrl(getUrl());
    }
}
