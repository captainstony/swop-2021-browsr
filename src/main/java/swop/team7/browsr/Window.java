package swop.team7.browsr;

import swop.team7.browsr.focus.KeyboardFocusManager;
import swop.team7.browsr.gui.canvaswindow.CanvasWindow;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.widgets.Modal;

import java.awt.*;
import java.util.EmptyStackException;
import java.util.Stack;

/**
 * Window holds the graphical representation of Browsr
 * <p>
 * Window propagates events through the Widget-tree
 */
public class Window extends CanvasWindow {
    private final Stack<Modal> modals;
    private WindowShownHandler shownHandler;

    public Window(WindowShownHandler shownHandler) {
        this();
        this.shownHandler = shownHandler;
    }

    public Window() {
        super("Browsr");
        this.modals = new Stack<>();
    }

    public void registerShownHandler(WindowShownHandler handler) {
        this.shownHandler = handler;
    }

    public void showModal(Modal root) {
        // reset focus
        KeyboardFocusManager.clearFocus();

        this.modals.push(root);
        this.requestLayout();
        this.requestRedraw();
    }

    public void closeModal() {
        this.modals.pop();
        this.requestLayout();
        this.requestRedraw();
    }

    public void requestRedraw() {
        repaint();
    }

    public Modal getModal() {
        try {
            return this.modals.peek();
        } catch (EmptyStackException e) {
            return null;
        }
    }

    public Size getSize() {
        return new Size(getWidth(), getHeight());
    }

    @Override
    protected void handleShown() {
        if (this.shownHandler != null)
            this.shownHandler.handleShown();
        this.requestLayout();
        this.requestRedraw();
    }

    public void requestLayout() {
        if (this.getModal() != null)
            this.getModal().layout(this.getSize());
    }

    @Override
    protected void paint(Graphics g) {
        if (this.getModal() != null)
            this.getModal().render(g);
    }

    @Override
    protected void handleResize() {
        this.requestLayout();
        this.requestRedraw();
    }

    @Override
    protected void handleMouseEvent(int id, int x, int y, int clickCount, int button, int modifiersEx) {
        super.handleMouseEvent(id, x, y, clickCount, button, modifiersEx);

        if (this.getModal() != null)
            this.getModal().handleMouseEvent(new MouseEvent(id, new Position(x, y), clickCount, button, modifiersEx));
    }

    @Override
    protected void handleKeyEvent(int id, int keyCode, char keyChar, int modifiersEx) {
        if (this.getModal() != null)
            this.getModal().handleKeyEvent(new KeyEvent(id, keyCode, keyChar, modifiersEx));
    }

    public interface WindowShownHandler {
        void handleShown();
    }
}
