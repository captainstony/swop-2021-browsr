package swop.team7.browsr.html.factory;

import swop.team7.browsr.BrowserController;
import swop.team7.browsr.gui.widgets.*;
import swop.team7.browsr.html.parser.objects.*;

import java.util.ArrayList;

/**
 * The WidgetFactory is responsible for converting {@link HtmlObject}s into {@link Widget}s.
 * <p>
 * A {@link BrowserController} must be passed to the constructor and is used to register event handlers
 * on the produced Widgets.
 */
public class WidgetFactory {
    private final BrowserController controller;
    Form activeForm;

    public WidgetFactory(BrowserController controller) {
        this.controller = controller;
    }

    /**
     * Creates a {@link Widget} from the given {@link HtmlObject} based on {@link HtmlObject#getObjectType()}.
     *
     *  <ul>
     *   <li>{@link HtmlObject.HtmlObjectType#HTML_A} produces a {@link Link}</li>
     *   <li>{@link HtmlObject.HtmlObjectType#HTML_TABLE} produces a {@link Table}.
     *      For each table cell createWidget is called recursively.</li>
     *   <li>{@link HtmlObject.HtmlObjectType#HTML_TEXT} produces a {@link Text}</li>
     *   <li>{@link HtmlObject.HtmlObjectType#HTML_FORM} produces a {@link Form}.
     *      createWidget is called recursively on the child</li>
     *   <li>{@link HtmlObject.HtmlObjectType#HTML_INPUT} produces a {@link InputField} or a {@link Button} depending
     *      on {@link HtmlInput#getType()}.</li>
     * </ul>
     */
    public Widget createWidget(HtmlObject object) {
        switch (object.getObjectType()) {
            case HTML_A:
                Link link = new Link(controller.getContext(), ((HtmlA) object).getText().getText(), ((HtmlA) object).getHref());
                link.registerUrlChangedHandler(this.controller);
                return link;
            case HTML_TABLE:
                ArrayList<ArrayList<Widget>> matrix = new ArrayList<>(object.getChildren().size());
                for (int i = 0; i < object.getChildren().size(); i++) matrix.add(new ArrayList<>());
                int i = 0;
                for (HtmlObject r : object.getChildren()) {
                    HtmlTr row = (HtmlTr) r;
                    int j = 0;
                    for (HtmlTd elem : row.getColumns()) {
                        matrix.get(i).add(createWidget(elem.getChild()));
                        j++;
                    }
                    i++;
                }

                return new Table(controller.getContext(), matrix);
            case HTML_TEXT:
                return new Text(controller.getContext(), ((HtmlText) object).getText());
            case HTML_FORM:
                Form form = new Form(controller.getContext(), ((HtmlForm) object).getAction(), null);
                activeForm = form;
                Widget child = createWidget(((HtmlForm) object).getChild());
                form.setChild(child);
                form.registerFormSubmitHandler(this.controller);
                activeForm = null;
                return form;
            case HTML_INPUT:
                if (((HtmlInput) object).getType() == HtmlInput.Type.SUBMIT) {
                    Button button = new Button(controller.getContext(), "Submit");
                    button.registerClickHandler(activeForm);
                    return button;
                } else {
                    InputField inputField = new InputField(controller.getContext(), ((HtmlInput) object).getName());
                    activeForm.registerTextInputField(inputField);
                    return inputField;
                }
            default:
                break;
        }

        return null;
    }
}
