package swop.team7.browsr.html.parser.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HtmlInput implements HtmlObject {
    private final Type type;
    private final String name;

    public HtmlInput(Type type, String name) {
        this.type = type;
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public List<HtmlObject> getChildren() {
        return new ArrayList<>();
    }

    @Override
    public HtmlObjectType getObjectType() {
        return HtmlObjectType.HTML_INPUT;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HtmlInput htmlInput = (HtmlInput) o;
        return getType() == htmlInput.getType() && Objects.equals(getName(), htmlInput.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getName());
    }

    public enum Type {
        TEXT,
        SUBMIT,
    }
}
