package swop.team7.browsr.html.parser.objects;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class HtmlTd implements HtmlObject {
    private final HtmlObject child;

    public HtmlTd(HtmlObject child) {
        this.child = child;
    }

    /**
     * @return A HtmlObject that is the content of this HtmlTd object.
     */
    public HtmlObject getChild() {
        return child;
    }

    @Override
    public List<HtmlObject> getChildren() {
        return Collections.singletonList(this.child);
    }

    @Override
    public HtmlObjectType getObjectType() {
        return HtmlObjectType.HTML_TD;
    }

    /**
     * Check whether both objects are the same.
     *
     * @param o An other object.
     * @return True if the contents (child) of this object is equal to the content of the other object.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HtmlTd htmlTd = (HtmlTd) o;
        return getChild().equals(htmlTd.getChild());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getChild());
    }
}
