package swop.team7.browsr.html.parser;

import swop.team7.browsr.html.parser.objects.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;

public class HtmlParser {
    HtmlLexer lexer;

    public HtmlParser(Reader reader) {
        lexer = new HtmlLexer(reader);
    }

    public HtmlParser(String input) {
        lexer = new HtmlLexer(new StringReader(input));
    }

    public HtmlParser(URL url) throws IOException {
        lexer = new HtmlLexer(new BufferedReader(new InputStreamReader(url.openStream())));
    }

    public HtmlObject parse() throws InvalidBrowsrDocumentException {
        return consumeBrowsrDocument();
    }

    void eatToken() {
        lexer.eatToken();
    }

    void fail() throws InvalidBrowsrDocumentException {
        throw new InvalidBrowsrDocumentException();
    }

    void assertTrue(boolean b) throws InvalidBrowsrDocumentException {
        if (!b)
            fail();
    }

    void assertToken(HtmlLexer.TokenType tokenType) throws InvalidBrowsrDocumentException {
        assertTrue(lexer.getTokenType() == tokenType);
    }

    void assertTokenValue(String value) throws InvalidBrowsrDocumentException {
        assertTrue(lexer.getTokenValue().equals(value));
    }

    String consumeToken(HtmlLexer.TokenType tokenType) throws InvalidBrowsrDocumentException {
        assertToken(tokenType);
        String result = lexer.getTokenValue();
        eatToken();
        return result;
    }

    void consumeToken(HtmlLexer.TokenType tokenType, String tokenValue) throws InvalidBrowsrDocumentException {
        assertToken(tokenType);
        assertTokenValue(tokenValue);
        eatToken();
    }

    void consumeOpenStartTag(String tagName) throws InvalidBrowsrDocumentException {
        consumeToken(HtmlLexer.TokenType.OPEN_START_TAG, tagName);
    }

    String consumeAttribute(String attributeName) throws InvalidBrowsrDocumentException {
        consumeToken(HtmlLexer.TokenType.IDENTIFIER, attributeName);
        consumeToken(HtmlLexer.TokenType.EQUALS);
        return consumeToken(HtmlLexer.TokenType.QUOTED_STRING);
    }

    Attribute consumeAttribute() throws InvalidBrowsrDocumentException {
        String key = consumeToken(HtmlLexer.TokenType.IDENTIFIER);
        consumeToken(HtmlLexer.TokenType.EQUALS);
        String value = consumeToken(HtmlLexer.TokenType.QUOTED_STRING);
        return new Attribute(key, value);
    }

    ArrayList<Attribute> consumeAttributes() throws InvalidBrowsrDocumentException {
        ArrayList<Attribute> attrs = new ArrayList<>();

        while (lexer.getTokenType() == HtmlLexer.TokenType.IDENTIFIER) {
            attrs.add(consumeAttribute());
        }

        return attrs;
    }

    HtmlObject consumeBrowsrDocument() throws InvalidBrowsrDocumentException {
        HtmlObject span = consumeContentSpan();
        assertToken(HtmlLexer.TokenType.END_OF_FILE);
        return span;
    }

    //ArrayList<Attribute> consumeAttributes

    String consumeTextSpan() throws InvalidBrowsrDocumentException {
        // When interpreting a text span as a single text string,
        // whitespace between text tokens is replaced by a single space character.
        StringBuilder text = new StringBuilder();
        boolean first = true;
        while (lexer.getTokenType() == HtmlLexer.TokenType.TEXT) {
            if (first)
                first = false;
            else
                text.append(' ');
            text.append(lexer.getTokenValue());
            eatToken();
        }
        return text.toString();
    }

    void consumeEndTag(String tagName) throws InvalidBrowsrDocumentException {
        consumeToken(HtmlLexer.TokenType.OPEN_END_TAG, tagName);
        consumeToken(HtmlLexer.TokenType.CLOSE_TAG);
    }

    HtmlA consumeHyperlink() throws InvalidBrowsrDocumentException {
        consumeOpenStartTag("a");
        String href = consumeAttribute("href");
        consumeToken(HtmlLexer.TokenType.CLOSE_TAG);
        String text = consumeTextSpan();
        consumeEndTag("a");
        return new HtmlA(href, new HtmlText(text));
    }

    void consumeStartTag(String tagName) throws InvalidBrowsrDocumentException {
        consumeOpenStartTag(tagName);
        consumeToken(HtmlLexer.TokenType.CLOSE_TAG);
    }

    HtmlTd consumeTableCell() throws InvalidBrowsrDocumentException {
        consumeStartTag("td");
        return new HtmlTd(consumeContentSpan());
    }

    HtmlTr consumeTableRow() throws InvalidBrowsrDocumentException {
        consumeStartTag("tr");

        ArrayList<HtmlTd> cells = new ArrayList<>();

        while (lexer.getTokenType() == HtmlLexer.TokenType.OPEN_START_TAG && lexer.getTokenValue().equals("td"))
            cells.add(consumeTableCell());

        return new HtmlTr(cells);
    }

    HtmlTable consumeTable() throws InvalidBrowsrDocumentException {
        consumeStartTag("table");

        ArrayList<HtmlTr> rows = new ArrayList<>();

        while (lexer.getTokenType() == HtmlLexer.TokenType.OPEN_START_TAG && lexer.getTokenValue().equals("tr"))
            rows.add(consumeTableRow());

        consumeEndTag("table");

        return new HtmlTable(rows);
    }

    private HtmlObject consumeInput() throws InvalidBrowsrDocumentException {
        consumeOpenStartTag("input");

        HtmlInput.Type type = null;
        String name = null;

        for (Attribute attr : consumeAttributes()) {
            switch (attr.key) {
                case "type" -> {
                    switch (attr.value) {
                        case "text" -> type = HtmlInput.Type.TEXT;
                        case "submit" -> type = HtmlInput.Type.SUBMIT;
                    }
                }
                case "name" -> name = attr.value;
            }
        }

        if (type == null || type == HtmlInput.Type.TEXT && name == null) {
            throw new InvalidBrowsrDocumentException();
        }

        consumeToken(HtmlLexer.TokenType.CLOSE_TAG);
        return new HtmlInput(type, name);
    }

    private HtmlObject consumeForm() throws InvalidBrowsrDocumentException {
        consumeOpenStartTag("form");
        String action = consumeAttribute("action");
        consumeToken(HtmlLexer.TokenType.CLOSE_TAG);
        HtmlObject content = consumeContentSpan();
        consumeEndTag("form");
        return new HtmlForm(content, action);
    }

    HtmlObject consumeContentSpan() throws InvalidBrowsrDocumentException {
        switch (lexer.getTokenType()) {
            case TEXT:
                return new HtmlText(consumeTextSpan());
            case OPEN_START_TAG:
                switch (lexer.getTokenValue()) {
                    case "a":
                        return consumeHyperlink();
                    case "table":
                        return consumeTable();
                    case "form":
                        return consumeForm();
                    case "input":
                        return consumeInput();
                    default:
                        fail();
                }
            default:
                fail();
        }

        return null;
    }

    static class Attribute {
        private final String key;
        private final String value;

        Attribute(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
