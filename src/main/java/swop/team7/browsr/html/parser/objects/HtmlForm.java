package swop.team7.browsr.html.parser.objects;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class HtmlForm implements HtmlObject {
    private final HtmlObject child;
    private final String action;


    public HtmlForm(HtmlObject child, String action) {
        this.child = child;
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public HtmlObject getChild() {
        return child;
    }

    @Override
    public List<HtmlObject> getChildren() {
        return Collections.singletonList(this.child);
    }

    @Override
    public HtmlObjectType getObjectType() {
        return HtmlObjectType.HTML_FORM;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HtmlForm htmlForm = (HtmlForm) o;
        return Objects.equals(getChild(), htmlForm.getChild()) && Objects.equals(action, htmlForm.action);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getChild(), action);
    }
}
