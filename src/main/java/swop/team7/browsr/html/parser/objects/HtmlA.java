package swop.team7.browsr.html.parser.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HtmlA implements HtmlObject {
    private final String href;
    private final HtmlText text;

    public HtmlA(String href, HtmlText text) {
        this.href = href;
        this.text = text;
    }

    /**
     * @return The target HREF from this A tag Html object.
     */
    public String getHref() {
        return href;
    }

    /**
     * @return The text from this A tag Html object.
     */
    public HtmlText getText() {
        return text;
    }

    @Override
    public List<HtmlObject> getChildren() {
        return new ArrayList<>();
    }

    @Override
    public HtmlObjectType getObjectType() {
        return HtmlObjectType.HTML_A;
    }

    /**
     * We check whether both objects are the same.
     *
     * @param o An other object to compare.
     * @return True if the text of both objects are the same.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HtmlA htmlA = (HtmlA) o;
        return getHref().equals(htmlA.getHref()) && getText().equals(htmlA.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHref(), getText());
    }
}
