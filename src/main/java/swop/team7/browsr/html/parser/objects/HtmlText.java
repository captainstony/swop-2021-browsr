package swop.team7.browsr.html.parser.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HtmlText implements HtmlObject {
    private final String text;

    public HtmlText(String text) {
        this.text = text;
    }

    /**
     * @return The text from this HtmlText object.
     */
    public String getText() {
        return text;
    }

    @Override
    public List<HtmlObject> getChildren() {
        return new ArrayList<>();
    }

    @Override
    public HtmlObjectType getObjectType() {
        return HtmlObjectType.HTML_TEXT;
    }

    /**
     * Check whether both objects are the same.
     *
     * @param o An other object.
     * @return True if both text objects have the same text.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HtmlText htmlText = (HtmlText) o;
        return getText().equals(htmlText.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getText());
    }
}
