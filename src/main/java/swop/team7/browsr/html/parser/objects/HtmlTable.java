package swop.team7.browsr.html.parser.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class HtmlTable implements HtmlObject {
    private final ArrayList<HtmlTr> rows;

    public HtmlTable(ArrayList<HtmlTr> rows) {
        this.rows = rows;
    }

    /**
     * @return A list containing all the rows from this table.
     */
    public ArrayList<HtmlTr> getRows() {
        return rows;
    }

    @Override
    public List<HtmlObject> getChildren() {
        return this.rows.stream().map(row -> (HtmlObject) row).collect(Collectors.toList());
    }

    @Override
    public HtmlObjectType getObjectType() {
        return HtmlObjectType.HTML_TABLE;
    }

    /**
     * We check if both objects are the same.
     *
     * @param o An other object.
     * @return True if both tables have the same rows.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HtmlTable htmlTable = (HtmlTable) o;
        return getRows().equals(htmlTable.getRows());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRows());
    }
}
