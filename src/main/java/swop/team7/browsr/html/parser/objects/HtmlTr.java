package swop.team7.browsr.html.parser.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class HtmlTr implements HtmlObject {
    private final ArrayList<HtmlTd> columns;

    public HtmlTr(ArrayList<HtmlTd> columns) {
        this.columns = columns;
    }

    public ArrayList<HtmlTd> getColumns() {
        return columns;
    }

    /**
     * @return A list containing all the HtmlObjects in this row.
     */
    @Override
    public List<HtmlObject> getChildren() {
        // TODO find better solution?
        return this.columns.stream().map(col -> (HtmlObject) col).collect(Collectors.toList());
    }

    @Override
    public HtmlObjectType getObjectType() {
        return HtmlObjectType.HTML_TR;
    }

    /**
     * Check whether both objects are the same.
     *
     * @param o An other object.
     * @return True if the columns of both objects are the same.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HtmlTr htmlTr = (HtmlTr) o;
        return getColumns().equals(htmlTr.getColumns());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getColumns());
    }
}
