package swop.team7.browsr.html.parser.objects;

import java.util.List;

/**
 * We convert the parsed objects in HTML objects, and HTML objects into widgets.
 * This is useful because HTML objects have a tree-like structure, and are easily converted into widgets which are easy to render.
 */
public interface HtmlObject {
    /**
     * The getChildren function contains a tree-like structure.
     *
     * @return A List containing all the HTML child objects.
     */
    List<HtmlObject> getChildren();

    /**
     * We assign every HTML object a type (enum).
     * This allows us to determine which implementation of HtmlObject we are using.
     * Reflection won't be necessary while using this scheme.
     *
     * @return The HTML object type.
     */
    HtmlObjectType getObjectType();

    enum HtmlObjectType {
        HTML_A,
        HTML_TABLE,
        HTML_TD,
        HTML_TR,
        HTML_TEXT,
        HTML_FORM,
        HTML_INPUT,
    }
}

