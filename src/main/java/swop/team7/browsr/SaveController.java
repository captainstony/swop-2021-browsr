package swop.team7.browsr;

import swop.team7.browsr.gui.modals.SaveModal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * SaveController is the controller instance which operates for the modal of the dialog screen used to save pages.
 */
public class SaveController extends Controller {
    private final String page;
    private SaveModal modal;

    protected SaveController(Window window, Context context, String page) {
        super(window, context);
        this.page = page;
    }

    public SaveModal getModal() {
        return this.modal;
    }

    /**
     * Show the modal for which this controller operates in the window.
     */
    public void showModal() {
        modal = new SaveModal(this.context);
        modal.getSaveButton().registerClickHandler(this::handleSubmit);
        modal.getCancelButton().registerClickHandler(this::handleCancel);
        this.window.showModal(modal);
    }

    /**
     * Handle the event where a page is submitted to be saved.
     */
    public void handleSubmit() {
        try {
            Files.createDirectories(Paths.get("./saved/"));
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File("./saved/", modal.getFilenameInput().getText())));
            writer.write(page);
            writer.close();
            this.window.closeModal();
        } catch (IOException e) {
            showErrorModal("Error: Unable to save file");
        }
    }

    /**
     * Handle the event where the saving of a page is canceled.
     */
    public void handleCancel() {
        System.out.println("Cancel");
        this.window.closeModal();
    }
}
