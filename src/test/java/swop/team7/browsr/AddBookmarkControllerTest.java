package swop.team7.browsr;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import swop.team7.browsr.focus.KeyboardFocusManager;
import swop.team7.browsr.gui.widgets.InputField;
import swop.team7.browsr.gui.widgets.Link;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class AddBookmarkControllerTest extends ControllerTestUtils {
    BrowserController controller;
    AddBookmarkController addBookmarkController;

    @BeforeEach
    @Override
    public void setup() {
        super.setup();
    }

    // Use case 4.5: Main
    @Test
    public void testAddBookmark() {
        addBookmarkControllerTestCommon();

        // Step 7: User clicks save button
        click(700, 380);

        verify(addBookmarkController).handleSubmit();

        // Step 8: New bookmark created
        verify(window).closeModal();
        ArrayList<Link> bookmarks = controller.getBookmarkBar().getBookmarks();
        Link last = bookmarks.get(bookmarks.size()-1);
        Assertions.assertEquals(last.getTarget(), "https://home.vanoverloop.xyz/browsrtest/simple_test.html");
        Assertions.assertEquals(last.getText(), "unit test bookmark");
    }

    // Use case 4.5: Extension 7.a
    @Test
    public void testCancelAddBookmark() {
        addBookmarkControllerTestCommon();

        // Step 7a:
        click(630, 380);
        verify(addBookmarkController).handleCancel();
        verify(window).closeModal();

        // The bookmark should not exist
        ArrayList<Link> bookmarks = controller.getBookmarkBar().getBookmarks();
        Link last = bookmarks.get(bookmarks.size()-1);
        Assertions.assertNotEquals(last.getTarget(), "https://home.vanoverloop.xyz/browsrtest/simple_test.html");
        Assertions.assertNotEquals(last.getText(), "unit test bookmark");
    }

    // Use case 4.5
    public void addBookmarkControllerTestCommon() {
        controller = spy(new BrowserController(window, context));
        controller.showModal();

        // Make sure we "catch" the adddBookmark controller when it is created
        when(controller.createAddBookmarkController(anyString())).thenAnswer((InvocationOnMock invocation) -> {
            addBookmarkController = spy(new AddBookmarkController(controller.window, controller.context, invocation.getArgument(0)));
            return addBookmarkController;
        });

        // Load a simple page for which a bookmark can be created
        controller.handleUrlChanged("https://home.vanoverloop.xyz/browsrtest/simple_test.html");

        // Step 1: Send Ctr+D
        window.handleKeyEvent(401, 68, 'd', 128);

        // Step 2: Showing dialog screen
        verify(controller).createAddBookmarkController("https://home.vanoverloop.xyz/browsrtest/simple_test.html");

        // The adBookmark controller should exist now
        Assertions.assertNotNull(addBookmarkController);

        // It should show a modal
        verify(addBookmarkController).showModal();

        // An addBookmark modal should be shown on the window
        verify(window).showModal(addBookmarkController.getModal());

        verifyDrawString("Name");
        verifyDrawString("URL ");
        verifyDrawString("Add Bookmark");
        verifyDrawString("Cancel");

        // Step 3: User clicks input field
        click(200, 170);

        InputField input = addBookmarkController.getModal().getNameInput();
        // Step 4: Text input has focus
        Assertions.assertTrue(KeyboardFocusManager.isFocussed(input.getTextInput()));

        // Step 5 & 6:
        verifyAppendText(input, "unit test bookmark");
    }
}
