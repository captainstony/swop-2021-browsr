package swop.team7.browsr;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.focus.FrameFocusManager;
import swop.team7.browsr.focus.KeyboardFocusManager;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.modals.BrowserModal;
import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.widgets.DocumentViewWidget;
import swop.team7.browsr.gui.widgets.Frame;
import swop.team7.browsr.gui.widgets.Split;

import javax.swing.*;

import static org.mockito.Mockito.*;

public class BrowserControllerTests extends ControllerTestUtils {
    BrowserController controller;

    @BeforeEach
    @Override
    public void setup() {
        super.setup();
        controller = spy(new BrowserController(window, context));
        controller.showModal();
    }

    private void verifyDrawBroswrTest() {
        verifyDrawString("HTML elements partially supported by Browsr:");
        verifyDrawString("Hyperlink anchors");
        verifyDrawString("Tables");
        verifyDrawString("Table rows");
        verifyDrawString("Table cells containing table data");
    }


    // Use case 4.1
    @Nested
    public class ActivateHyperlink {
        // Main success scenario
        @Test
        public void testClickLink() {
            // Verify that the test page is shown
            verifyDrawString("Click here to view the first test page");

            // Step 1: User clicks hyperlink
            int bookmarkBarHeight = controller.getBookmarkBar().getSize().getHeight();
            click(5, 80 + bookmarkBarHeight);

            // Step 2:
            // Make sure the handler is called, TODO test address composition according to spec
            verify(controller).handleUrlChanged("https://people.cs.kuleuven.be/~bart.jacobs/browsrtest.html");

            // Verify that the Browsr test page is drawn
            verifyDrawBroswrTest();

            // Verify that the AddressBar is drawing this string
            verify(graphics, atLeastOnce()).drawString(eq("https://people.cs.kuleuven.be/~bart.jacobs/browsrtest.html"), anyInt(), anyInt());
        }

        // Extension 2a: Loading error
        @Test
        public void testClickLinkError() {
            // Verify that the test page is shown
            verifyDrawString("Click here to view the first test page");

            // Load the invalid link test page
            controller.setLocalPage("html/invalid_link_test.html");
            window.requestLayout();
            window.requestRedraw();

            // Verify that it is loaded
            verifyDrawString("Malformed URL");

            // Step 1: User clicks hyperlink
            int bookmarkBarHeight = controller.getBookmarkBar().getSize().getHeight();
            click(5, 42 + bookmarkBarHeight);

            // Step 2:
            // Make sure the handler is called,
            verify(controller).handleUrlChanged("htps:/people.cs.kuleuven.be/~bart.jacobs/browsrtest.html");

            // Step 2a:
            // Make sure the right error document is shown
            verifyDrawString("Error: The address entered is not valid.");
        }
    }

    // Use case 4.2
    @Nested
    public class EnterURL {
        // Main success scenario
        @Test
        public void testEnterUrl() {
            // Verify that the test page is shown
            verifyDrawString("Click here to view the first test page");

            // Step 1: User clicks address bar
            click(5, 5);

            // Step 2:
            // Verify that the address bar has focus
            Assertions.assertTrue(KeyboardFocusManager.isFocussed(controller.getAddressBar().getTextInput()));
            // Verify that the address bar text is highlighted
            Assertions.assertTrue(controller.getAddressBar().getTextInput().isHighlighted());
            Assertions.assertEquals(controller.getAddressBar().getTextInput().getHighlightedRange().get(0), Integer.valueOf(0));
            Assertions.assertEquals(controller.getAddressBar().getTextInput().getHighlightedRange().get(1), Integer.valueOf(controller.getAddressBar().getText().length()));
            // Verify that the cursor and highlighted text are shown
            verify(controller.getContext()).requestRepaint();

            // Step 3 & 4:

            // Step 5:
            // User presses enter
            window.handleKeyEvent(401, KeyEvent.ENTER, '\n', 0);

            // Step 6: Verify that the system loads the document
            verify(controller, times(2)).handleUrlChanged(anyString());
        }

        // Extension 5a: User presses escape
        @Test
        public void testAddressBarCancel() {
            // Verify that the test page is shown
            verifyDrawString("Click here to view the first test page");

            // Step 1: User clicks address bar
            click(5, 5);

            // Step 2:
            // Verify that the address bar has focus
            Assertions.assertTrue(KeyboardFocusManager.isFocussed(controller.getAddressBar().getTextInput()));
            // Verify that the address bar text is highlighted
            Assertions.assertTrue(controller.getAddressBar().getTextInput().isHighlighted());
            Assertions.assertEquals(controller.getAddressBar().getTextInput().getHighlightedRange().get(0), Integer.valueOf(0));
            Assertions.assertEquals(controller.getAddressBar().getTextInput().getHighlightedRange().get(1), Integer.valueOf(controller.getAddressBar().getText().length()));
            // Verify that the cursor and highlighted text are shown
            verify(controller.getContext()).requestRepaint();

            // Step 3 & 4:

            // Step 5.a:
            // User presses escape
            window.handleKeyEvent(401, KeyEvent.ESCAPE, '\n', 0);

            // Step 6: Verify that the system loads the document
            verify(controller, atMostOnce()).handleUrlChanged(anyString());
        }

        // Extension 6a: URL malformed
        @Test
        public void testEnterMalformedURL() {
            // Verify that the test page is shown
            verifyDrawString("Click here to view the first test page");

            // Step 1: User clicks address bar
            click(5, 5);

            // Step 2:
            // Verify that the address bar has focus
            Assertions.assertTrue(KeyboardFocusManager.isFocussed(controller.getAddressBar().getTextInput()));
            // Verify that the address bar text is highlighted
            Assertions.assertTrue(controller.getAddressBar().getTextInput().isHighlighted());
            Assertions.assertEquals(controller.getAddressBar().getTextInput().getHighlightedRange().get(0), Integer.valueOf(0));
            Assertions.assertEquals(controller.getAddressBar().getTextInput().getHighlightedRange().get(1), Integer.valueOf(controller.getAddressBar().getText().length()));
            // Verify that the cursor and highlighted text are shown
            verify(controller.getContext()).requestRepaint();

            controller.getAddressBar().setUrl("test");

            // Step 3 & 4:

            // Step 5:
            // User clicks outside the address bar
            click(100, 100);

            // Step 6.a: Verify that the system loads the error document
            verify(controller, times(2)).handleUrlChanged(anyString());
            verifyDrawString("Error: The address entered is not valid.");
        }
    }

    // Use case 4.3
    @Nested
    public class Form {
        @Test
        public void formSubmit() {
            controller.handleUrlChanged("https://people.cs.kuleuven.be/~bart.jacobs/swop/browsrformtest.html");

            //Type the strings in the respective input fields.
            //Step 1,2,3,4:
            click(50, 105);
            typeString("string2");

            click(50, 145);
            typeString("string1");

            //Click on the submit button.
            //Step 5:
            click(23, 185);

            //Check if the navigation is correct.
            //Step 6:
            Assertions.assertEquals(controller.getAddressBar().getText(), "https://people.cs.kuleuven.be/~bart.jacobs/swop/browsrformactiontest.php?max_nb_results=string1&starts_with=string2");
        }
    }

    // Use case 4.7
    @Nested
    public class SplitPane {
        // Split pane horizontally (Ctrl+H)
        @Test
        public void splitHorizontal() {
            BrowserModal modal = controller.getModal();
            Frame initialFrame = FrameFocusManager.getFocussed();

            // Step 1: user presses Ctrl+H
            window.handleKeyEvent(401, 72, 'h', 128);

            // Step 2: the leaf pane containing the focussed frame is replaced by a horizontal split pane
            DocumentViewWidget splitPane = modal.getDocumentViewRoot();
            Assertions.assertTrue(splitPane instanceof Split);
            Assertions.assertEquals(((Split)splitPane).getOrientation(), Orientation.Horizontal);

            // The leaf pane containing the initial focussed frame is the child of the split pane
            Assertions.assertEquals(((Split)splitPane).getFirst(), initialFrame);

            // The sibling leaf pane shows the same page as the frame containing the initial focussed frame
            DocumentViewWidget secondFrame = ((Split)splitPane).otherChild(initialFrame);
            Assertions.assertTrue(secondFrame instanceof Frame);
            Assertions.assertEquals(initialFrame.getUrl(), ((Frame)secondFrame).getUrl());
        }

        // Split pane vertically (Ctrl+V)
        @Test
        public void splitVertical() {
            BrowserModal modal = controller.getModal();
            Frame initialFrame = FrameFocusManager.getFocussed();

            // Step 1: user presses Ctrl+V
            window.handleKeyEvent(401, 86, 'v', 128);

            // Step 2: the leaf pane containing the focussed frame is replaced by a vertical split pane
            DocumentViewWidget splitPane = modal.getDocumentViewRoot();
            Assertions.assertTrue(splitPane instanceof Split);
            Assertions.assertEquals(((Split)splitPane).getOrientation(), Orientation.Vertical);

            // The leaf pane containing the initial focussed frame is the child of the split pane
            Assertions.assertEquals(((Split)splitPane).getFirst(), initialFrame);

            // The sibling leaf pane shows the same page as the frame containing the initial focussed frame
            DocumentViewWidget secondFrame = ((Split)splitPane).otherChild(initialFrame);
            Assertions.assertTrue(secondFrame instanceof Frame);
            Assertions.assertEquals(initialFrame.getUrl(), ((Frame)secondFrame).getUrl());
        }
    }

    // Use case 4.8
    @Nested
    public class CloseFrame {
        // Main success scenario
        @Test
        public void closeFrame() {
            BrowserModal modal = controller.getModal();

            // Send Ctrl+H to split horizontally
            window.handleKeyEvent(401, 72, 'h', 128);

            // The root pane is a split pane
            DocumentViewWidget splitRoot = modal.getDocumentViewRoot();
            Assertions.assertTrue(splitRoot instanceof Split);

            Frame focussedFrame = FrameFocusManager.getFocussed();

            // The root pane contains the focussed widget
            Assertions.assertEquals(((Split)splitRoot).otherChild(((Split)splitRoot).getFirst()), focussedFrame);

            DocumentViewWidget otherChild = ((Split)splitRoot).otherChild(focussedFrame);

            // Step 1: user presses Ctrl+X
            window.handleKeyEvent(401, 88, 'x', 128);

            // Step 2: The parent pane of the pane containing the focussed frame is replaced by its sibling
            Assertions.assertEquals(modal.getDocumentViewRoot(), otherChild);

            // Some other frame is focussed
            Assertions.assertNotNull(FrameFocusManager.getFocussed());
        }

        // Extension 2a: root frame closed
        @Test
        public void closeRootFrame() {
            BrowserModal modal = controller.getModal();
            Frame focussedFrame = FrameFocusManager.getFocussed();

            // The leaf pane containing the focussed frame is the root pane
            Assertions.assertEquals(modal.getDocumentViewRoot(), focussedFrame);

            // Step 1: user presses Ctrl+X
            window.handleKeyEvent(401, 88, 'x', 128);

            // Step 2a: The root pane is replaced by a pane showing the welcome page
            DocumentViewWidget newFrame = modal.getDocumentViewRoot();
            Assertions.assertTrue(newFrame instanceof Frame);
            Assertions.assertEquals(((Frame)newFrame).getUrl(), "file://html/welcome.html");
        }
    }

    // Use case 4.9
    @Nested
    public class SelectFrame {
        @Test
        public void selectFrame() {
            Frame currentFocussed = FrameFocusManager.getFocussed();

            // Send Ctrl+H to split horizontally
            window.handleKeyEvent(401, 72, 'h', 128);

            // The new frame on the right is now focussed
            Assertions.assertNotEquals(FrameFocusManager.getFocussed(), currentFocussed);

            // Step 1: user clicks the left frame
            click(20, 300);

            // Step 2: the left frame is now focussed again
            Assertions.assertEquals(FrameFocusManager.getFocussed(), currentFocussed);
        }
    }

    // Use case 4.10
    @Nested
    public class DragSeparator {
        @Test
        public void dragSeparator() {
            // Send Ctrl+H to split horizontally
            window.handleKeyEvent(401, 72, 'h', 128);

            // Step 1: user drags the separator
            dragSeparatorHorizontal(200, 400, 600, 2);

            // Step 2: layout updates

            // The frame on the right is focussed
            Assertions.assertEquals(new Size(200 - 5, 430), FrameFocusManager.getFocussed().getSize());

            click(10, 200);

            // The frame on the left is focussed
            Assertions.assertEquals(new Size(600 - 5, 430), FrameFocusManager.getFocussed().getSize());
        }
    }

    // Use case 4.11
    // ---> See ScrollTest.java
}