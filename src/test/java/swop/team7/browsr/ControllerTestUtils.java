package swop.team7.browsr;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.widgets.InputField;
import swop.team7.browsr.gui.widgets.TextInputWidget;

import javax.swing.*;
import java.awt.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public abstract class ControllerTestUtils {
    Window window;
    Context context;
    Graphics graphics;
    FontMetrics metrics;

    @BeforeEach
    public void setup() {
        window = spy(new Window());
        context = spy(new Context(window));
        graphics = mock(Graphics.class);
        metrics = mock(FontMetrics.class);

        when(metrics.stringWidth(any())).thenReturn(30);
        when(metrics.getHeight()).thenReturn(20);
        when(metrics.getAscent()).thenReturn(5);

        when(graphics.create(anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(graphics);
        when(graphics.getFontMetrics()).thenReturn(metrics);

        doAnswer(invocationOnMock -> {
            window.paint(graphics);
            return null;
        }).when(window).requestRedraw();

        doReturn(metrics).when(window).getFontMetrics(any());
        doReturn(800).when(window).getWidth();
        doReturn(500).when(window).getHeight();

        window.handleShown();
    }

    protected void verifyDrawString(String string) {
        verify(graphics, atLeastOnce()).drawString(eq(string), anyInt(), anyInt());
    }

    protected void verifyAppendText(TextInputWidget input, String text) {
        for (char c : text.toCharArray()) {
            String currentText = input.getText();
            KeyStroke ks = KeyStroke.getKeyStroke(c, 0);
            window.handleKeyEvent(401, ks.getKeyCode(), c, 0);
            Assertions.assertEquals(currentText + c, input.getText());
        }

        Assertions.assertEquals(text, input.getText());
    }

    protected void typeString(String text) {
        for (char c : text.toCharArray()) {
            KeyStroke ks = KeyStroke.getKeyStroke(c, 0);
            window.handleKeyEvent(401, ks.getKeyCode(), c, 0);
        }
    }

    protected void click(int x, int y) {
        window.handleMouseEvent(501, x, y, 1, 1, 0);
        window.handleMouseEvent(502, x, y, 1, 1, 0);
        window.handleMouseEvent(500, x, y, 1, 1, 0);
    }

    protected void dragMouse(int x, int y) {
        window.handleMouseEvent(506, x, y, 1, 0, 0);
    }

    protected void dragSeparatorHorizontal(int y, int from, int to, int step) {
        if (from < to) {
            for (int current = from; current <= to; current += step) {
                dragMouse(current, y);
            }
        } else {
            for (int current = from; current >= to; current -= step) {
                dragMouse(current, y);
            }
        }
    }
}
