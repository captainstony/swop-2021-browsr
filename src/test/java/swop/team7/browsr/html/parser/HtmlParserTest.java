package swop.team7.browsr.html.parser;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.html.parser.HtmlParser;
import swop.team7.browsr.html.parser.InvalidBrowsrDocumentException;
import swop.team7.browsr.html.parser.objects.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class HtmlParserTest {
    void assertParse(String html, HtmlObject expectedObject) throws InvalidBrowsrDocumentException {
        HtmlParser parser = new HtmlParser(html);
        Assertions.assertEquals(
                expectedObject,
                parser.parse()
        );
    }

    void assertParse(URL html, HtmlObject expectedObject) throws InvalidBrowsrDocumentException, IOException {
        HtmlParser parser = new HtmlParser(html);
        Assertions.assertEquals(
                expectedObject,
                parser.parse()
        );
    }

    void assertParseFails(String html) {
        HtmlParser parser = new HtmlParser(html);
        Assertions.assertThrows(InvalidBrowsrDocumentException.class, parser::parse);
    }

    @Test
    void testHtmlText() throws InvalidBrowsrDocumentException {
        assertParse(
                "Hello World!",
                new HtmlText("Hello World!")
        );
    }

    @Test
    void testHtmlA() throws InvalidBrowsrDocumentException {
        assertParse(
                """
                <a href="target">text</a>
                """,
                new HtmlA("target", new HtmlText("text"))
        );
    }

    @Test
    void testHtmlANoHref() throws InvalidBrowsrDocumentException {
        assertParseFails("""
                        <a>text</a>
                        """);
    }

    @Test
    void testHtmlTdWithoutTr() {
        assertParseFails("""
                        <td>
                        """);
    }

    @Test
    void testHtmlTrWithoutTable() {
        assertParseFails("""
                        <tr><td>
                        """);
    }

    @Test
    void testHtmlTable() throws InvalidBrowsrDocumentException {
        assertParse(
                """
                <table>
                    <tr><td>a<td>b
                    <tr><td>c<td>d 
                </table>
                """,
                new HtmlTable(new ArrayList<>(Arrays.asList(
                        new HtmlTr(new ArrayList<>(Arrays.asList(
                                new HtmlTd(new HtmlText("a")),
                                new HtmlTd(new HtmlText("b"))
                        ))),
                        new HtmlTr(new ArrayList<>(Arrays.asList(
                                new HtmlTd(new HtmlText("c")),
                                new HtmlTd(new HtmlText("d"))
                        )))
                )))
        );
    }

    @Test
    void testHtmlDocument() throws InvalidBrowsrDocumentException, IOException {
        assertParse(
                new URL(new URL("https://people.cs.kuleuven.be/~bart.jacobs/"), "browsrtest.html"),
                new HtmlTable(new ArrayList<>(Arrays.asList(
                        new HtmlTr(new ArrayList<>(Collections.singletonList(
                                new HtmlTd(new HtmlText("HTML elements partially supported by Browsr:"))
                        ))),
                        new HtmlTr(new ArrayList<>(Collections.singletonList(
                                new HtmlTd(
                                        new HtmlTable(new ArrayList<>(Arrays.asList(
                                                new HtmlTr(new ArrayList<>(Arrays.asList(
                                                        new HtmlTd(new HtmlA("a.html", new HtmlText("a"))),
                                                        new HtmlTd(new HtmlText("Hyperlink anchors"))
                                                ))),
                                                new HtmlTr(new ArrayList<>(Arrays.asList(
                                                        new HtmlTd(new HtmlA("table.html", new HtmlText("table"))),
                                                        new HtmlTd(new HtmlText("Tables"))
                                                ))),
                                                new HtmlTr(new ArrayList<>(Arrays.asList(
                                                        new HtmlTd(new HtmlA("tr.html", new HtmlText("tr"))),
                                                        new HtmlTd(new HtmlText("Table rows"))
                                                ))),
                                                new HtmlTr(new ArrayList<>(Arrays.asList(
                                                        new HtmlTd(new HtmlA("td.html", new HtmlText("td"))),
                                                        new HtmlTd(new HtmlText("Table cells containing table data"))
                                                )))
                                        )))
                                )
                        )))
                )))
        );
    }

    @Test
    void testInputTextValid() throws InvalidBrowsrDocumentException {
        assertParse(
                """
                <input type="text" name="password-field">
                """,
                new HtmlInput(HtmlInput.Type.TEXT, "password-field")
        );
    }

    @Test
    void testInputTextInvalid() {
        assertParseFails("""
            <input type="text">
            """);
    }

    @Test
    void testInputSubmitValid() throws InvalidBrowsrDocumentException {
        assertParse(
                """
                <input type="submit">
                """,
                new HtmlInput(HtmlInput.Type.SUBMIT, null)
        );
    }

    @Test
    void testInputNoTypeInvalid() {
        assertParseFails("""
            <input name="i-dont-have-a-type">
            """);
    }

    @Test
    void testForm() throws InvalidBrowsrDocumentException {
        assertParse(
                """
                <form action="submit.php"> 
                    This is a form 
                </form>
                """,
                new HtmlForm(
                        new HtmlText("This is a form"),
                        "submit.php"
                )
        );
    }

    @Test
    void testFormNoAction() {
        assertParseFails("""
            <form> 
                This is a form 
            </form>
            """);
    }
}
