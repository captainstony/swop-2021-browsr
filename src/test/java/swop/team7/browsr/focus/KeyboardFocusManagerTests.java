package swop.team7.browsr.focus;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import swop.team7.browsr.Window;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.gui.widgets.Modal;
import swop.team7.browsr.gui.widgets.TextInputWidget;

import static org.mockito.Mockito.*;

public class KeyboardFocusManagerTests {

    WidgetContext context = mock(WidgetContext.class);
    class MockWidget extends TextInputWidget {
        public MockWidget() {
            super(context);
        }
    }

    TextInputWidget buildMockWidget() {
        TextInputWidget widget = spy(new MockWidget());
        return widget;
    }

    class MockModal extends Modal {
        public MockModal() {
            super(context);
        }
    }

    MockModal buildMockModal(){
        Modal modal = spy(new MockModal());
        return (MockModal) modal;
    }


    class MockWindow extends Window {
        public MockWindow() {
            super();
        }
    }

    MockWindow buildMockWindow(){
        MockWindow window = spy(new MockWindow());
        doNothing().when(window).requestLayout();
        doNothing().when(window).requestRedraw();
        return window;
    }

    public void setFocussed(TextInputWidget widget) {
        KeyboardFocusManager.requestFocus(widget);
    }

    public boolean isFocussed(TextInputWidget widget) {
       return KeyboardFocusManager.isFocussed(widget);
    }

    public void sendEventToFocussed(KeyEvent event) {
        KeyboardFocusManager.sendKeyEventToFocussed(event);
    }

    @Test
    public void basicTest() {
        TextInputWidget widget1 = this.buildMockWidget();
        TextInputWidget widget2 = this.buildMockWidget();

        assert !this.isFocussed(widget1);
        assert !this.isFocussed(widget2);

        this.setFocussed(widget1);
        assert this.isFocussed(widget1);
        assert !this.isFocussed(widget2);

        this.setFocussed(widget2);
        assert !this.isFocussed(widget1);
        assert this.isFocussed(widget2);
    }

    @Test
    public void inputUnfocusedAfterShowModal() {
        TextInputWidget inputWidget = this.buildMockWidget();
        Window window = this.buildMockWindow();
        Modal modal = this.buildMockModal();

        this.setFocussed(inputWidget);
        assert this.isFocussed(inputWidget);

        window.showModal(modal);
        assert !this.isFocussed(inputWidget);
    }

    @Test
    public void keyEventsToFocussedWidget() {
        TextInputWidget widget1 = this.buildMockWidget();

        KeyEvent event = mock(KeyEvent.class);

        this.sendEventToFocussed(event);
        verify(widget1, never()).handleKeyEvent(event);

        this.setFocussed(widget1);
        this.sendEventToFocussed(event);
        verify(widget1).handleKeyEvent(event);

        KeyboardFocusManager.clearFocus();
        this.sendEventToFocussed(event);
        verify(widget1, Mockito.times(1)).handleKeyEvent(event);
    }
}
