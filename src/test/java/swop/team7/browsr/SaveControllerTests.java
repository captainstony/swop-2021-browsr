package swop.team7.browsr;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import swop.team7.browsr.focus.KeyboardFocusManager;
import swop.team7.browsr.gui.widgets.InputField;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.mockito.Mockito.*;

public class SaveControllerTests extends ControllerTestUtils {
    BrowserController controller;
    SaveController saveController;

    @BeforeEach
    @Override
    public void setup() {
        super.setup();
        Path file = Path.of("unit_test_page.html");
        try {
            Files.delete(file);
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    // Use case 4.6: Main
    @Test
    public void testSavePage() throws IOException {
        saveControllerTestCommon();

        // Step 7: User clicks save button
        click(560, 170 + 210);
        verify(saveController).handleSubmit();

        // Step 8: New file created
        verify(window).closeModal();
        Path file = Path.of("./saved/unit_test_page.html");
        String fileContent = Files.readString(file);
        Files.delete(file);
        Assertions.assertEquals("This is a test page", fileContent);
    }

    // Use case 4.6: Extension 7a
    @Test
    public void testCancelSave() {
        saveControllerTestCommon();

        // Step 7a:
        click(490, 170 + 210);
        verify(saveController).handleCancel();
        verify(window).closeModal();

        // This file should not exist
        Path file = Path.of("unit_test_page.html");
        Assertions.assertThrows(IOException.class, () -> Files.readString(file));
    }

    // Use case 4.6
    public void saveControllerTestCommon() {
        controller = spy(new BrowserController(window, context));
        controller.showModal();

        // Make sure we "catch" the save controller when it is created
        when(controller.createSaveController(anyString())).thenAnswer((InvocationOnMock invocation) -> {
            this.saveController = spy(new SaveController(controller.window, controller.context, invocation.getArgument(0)));
            return saveController;
        });

        // Load a simple page that can be saved
        controller.handleUrlChanged("https://home.vanoverloop.xyz/browsrtest/simple_test.html");

        // Step 1: Send Ctrl-S key event
        window.handleKeyEvent(401, 83, '', 128);

        // Step 2: Showing dialog screen
        verify(controller).createSaveController("This is a test page");

        // The save controller should exist now
        Assertions.assertNotNull(saveController);

        // It should show a modal
        verify(saveController).showModal();

        // A save modal should be shown on the window
        verify(window).showModal(saveController.getModal());

        verifyDrawString("Save As");
        verifyDrawString("File name: ");
        verifyDrawString("Save");
        verifyDrawString("Cancel");

        // Step 3: User clicks input field
        click(400, 170);

        InputField input = saveController.getModal().getFilenameInput();
        // Step 4: Text input has focus
        Assertions.assertTrue(KeyboardFocusManager.isFocussed(input.getTextInput()));

        // Step 5 & 6:
        verifyAppendText(input, "unit_test_page.html");
    }
}
