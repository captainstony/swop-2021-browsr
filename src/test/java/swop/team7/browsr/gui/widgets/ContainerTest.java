package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

import static org.mockito.Mockito.*;

public class ContainerTest {
    @Test
    public void testContainerRender() {
        WidgetContext context = mock(WidgetContext.class);
        Widget child = mock(Widget.class);
        Size childSize = new Size(20, 20);
        when(child.getSize()).thenReturn(childSize);
        Container container = new Container(context, child);
        Size size = new Size(69, 42);
        container.layout(size);

        verify(child).layout(size);

        // Container has the same size as its child
        Assertions.assertEquals(childSize, container.getSize());

        Graphics gfx = mock(Graphics.class);
        container.render(gfx);

        // Should get the same graphics, not a smaller child object of graphics
        verify(child).render(gfx);
    }

    @Test
    public void testContainerMouseEvents() {
        WidgetContext context = mock(WidgetContext.class);
        Widget child = mock(Widget.class);
        Container container = new Container(context);
        container.setChild(child);

        MouseEvent mouseEvent = mock(MouseEvent.class);
        container.handleMouseEvent(mouseEvent);
        verify(child).handleMouseEvent(mouseEvent);
    }
}
