package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import static org.mockito.Mockito.*;

public class ConstrainedContainerTest {
    @Test
    public void testLayout() {
        WidgetContext ctx = mock(WidgetContext.class);
        Widget child = mock(Widget.class);
        when(child.getSize()).thenReturn(new Size(10, 10));
        Size size = new Size(50, 50);
        ConstrainedContainer container = new ConstrainedContainer(ctx, child, size);

        // Container size smaller than layout max size
        container.layout(new Size(500, 500));
        verify(child).layout(size);

        // Layout max size smaller than container size
        container.layout(new Size(20, 30));
        verify(child).layout(new Size(20, 30));
    }
}
