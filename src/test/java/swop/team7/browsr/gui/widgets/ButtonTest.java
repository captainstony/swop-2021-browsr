package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.Context;
import swop.team7.browsr.ControllerTestUtils;
import swop.team7.browsr.Window;
import swop.team7.browsr.handlers.ButtonClickHandler;


import java.awt.*;

import static org.mockito.Mockito.*;

public class ButtonTest  {
    ButtonClickHandler form;
    Button button;
    Window window;
    Context context;
    Graphics graphics;
    FontMetrics metrics;
    @BeforeEach
    public void setup() {
        window = spy(new Window());
        context = spy(new Context(window));
        graphics = mock(Graphics.class);
        metrics = mock(FontMetrics.class);

        when(metrics.stringWidth(any())).thenReturn(30);
        when(metrics.getHeight()).thenReturn(20);
        when(metrics.getAscent()).thenReturn(5);

        when(graphics.create(anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(graphics);
        when(graphics.getFontMetrics()).thenReturn(metrics);


        doReturn(metrics).when(window).getFontMetrics(any());
        doReturn(800).when(window).getWidth();
        doReturn(500).when(window).getHeight();

        button = new Button(context,"this is a test button");

        form = mock(Form.class);
        button.registerClickHandler(form);
    }

    @Test
    public void buttonClickTest() {
        //When the button is clicked, the form will handle the click
        button.getClickHandler().handleClick();
        verify(form).handleClick();
    }

    @Test
    public void correctRender(){
        button.render(graphics);
        verify(graphics).drawString(eq("this is a test button"),anyInt(),anyInt());
    }

}
