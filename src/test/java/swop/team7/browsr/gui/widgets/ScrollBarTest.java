package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class ScrollBarTest {
    ScrollBar scrollBarHorizontal;
    ScrollBar scrollBarVertical;
    Widget child;
    WidgetContext context;
    Graphics gfx;
    ScrollableContainer container;

    @BeforeEach
    void setup() {
        context = mock(WidgetContext.class);
        gfx = mock(Graphics.class);

        child = mock(Widget.class);
        when(child.getSize()).thenReturn(new Size(500, 500));

        container = spy(new ScrollableContainer(context, child));
        container.registerScrollBar(Orientation.Horizontal);
        container.registerScrollBar(Orientation.Vertical);

        Graphics gfx = mock(Graphics.class);
        when(gfx.create(anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(gfx);
        container.layout(new Size(250, 250));
        container.render(gfx);


        // Does initiate correct
        assert (container.getHorizontalScrollBar() != null);
        scrollBarHorizontal = container.getHorizontalScrollBar();
        assert (scrollBarHorizontal.getOffset() == 0);


        // Does initiate correct
        assert (container.getVerticalScrollBar() != null);
        scrollBarVertical = container.getVerticalScrollBar();
        assert (scrollBarVertical.getOffset() == 0);
    }

    void dragMouse(int x, int y, Widget widget) {
        widget.handleMouseEvent(new MouseEvent(MouseEvent.Type.DRAGGED, new Position(x, y), 1, 0, Modifier.NONE));
    }

    @Test
    public void outOfBounds() {
        // VERTICAL
        int maxOffset = scrollBarVertical.getBarLength() - scrollBarVertical.getScrollerLength();

        // Manual offset test
        scrollBarVertical.setOffset(scrollBarVertical.getBarLength() + 100);
        assert (scrollBarVertical.getOffset() == maxOffset);
        scrollBarVertical.setOffset(-100);
        assert (scrollBarVertical.getOffset() == 0);

        // HandleMouseEvent
        dragMouse(245, 21, container);
        assert (scrollBarVertical.getOffset() == 0);
        dragMouse(245, scrollBarVertical.getBarLength() - scrollBarVertical.getScrollerLength() / 2 + 1, container);
        assert (scrollBarVertical.getOffset() == maxOffset);


        // HORIZONTAL
        maxOffset = scrollBarHorizontal.getBarLength() - scrollBarHorizontal.getScrollerLength();

        // Manual offset test
        scrollBarHorizontal.setOffset(scrollBarHorizontal.getBarLength() + 100);
        assert (scrollBarHorizontal.getOffset() == maxOffset);
        scrollBarHorizontal.setOffset(-100);
        assert (scrollBarHorizontal.getOffset() == 0);

        // HandleMouseEvent
        dragMouse(21, 245, container);
        assert (scrollBarHorizontal.getOffset() == 0);
        dragMouse(scrollBarHorizontal.getBarLength() - scrollBarHorizontal.getScrollerLength() / 2 + 1, 245, container);
        assert (scrollBarHorizontal.getOffset() == maxOffset);
    }

    @Test
    public void fullDragHorizontal() {
        int maxOffset = scrollBarHorizontal.getBarLength() - scrollBarHorizontal.getScrollerLength();
        int maxDrag = scrollBarHorizontal.getBarLength() - scrollBarHorizontal.getScrollerLength() / 2;

        for (int i = scrollBarHorizontal.getScrollerLength() / 2; i < maxDrag; i++) {
            dragMouse(i, 245, container);
            assert (scrollBarHorizontal.getOffset() == i - scrollBarHorizontal.getScrollerLength() / 2);
        }
    }

    @Test
    public void fullDragVertical() {
        int maxOffset = scrollBarVertical.getBarLength() - scrollBarVertical.getScrollerLength();
        int maxDrag = scrollBarVertical.getBarLength() - scrollBarVertical.getScrollerLength() / 2;

        for (int i = scrollBarVertical.getScrollerLength() / 2; i < maxDrag; i++) {
            dragMouse(245, i, container);
            assert (scrollBarVertical.getOffset() == i - scrollBarVertical.getScrollerLength() / 2);
        }
    }

}
