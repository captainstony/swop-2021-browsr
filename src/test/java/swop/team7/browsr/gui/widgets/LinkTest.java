package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;
import swop.team7.browsr.handlers.UrlChangedHandler;

import java.awt.*;

import static org.mockito.Mockito.*;

public class LinkTest {
    static class Handler implements UrlChangedHandler {
        String url;

        @Override
        public void handleUrlChanged(String url) {
            this.url = url;
        }
    }

    @Test
    public void testHandler() {
        WidgetContext context = mock(WidgetContext.class);
        when(context.getDefaultFont(anyInt())).thenReturn(new Font(Font.DIALOG, Font.PLAIN, 12));
        Link link = new Link(context, "click me", "to go here");
        link.setSize(new Size(5, 5));
        Handler handler = new Handler();
        link.registerUrlChangedHandler(handler);
        link.handleMouseEvent(new MouseEvent(500, new Position(1, 1), 1, 1, 0));
        Assertions.assertEquals("to go here", handler.url);
    }
}
