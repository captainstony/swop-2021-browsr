package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

import static org.mockito.Mockito.*;

public class ScrollTest {
    WidgetContext context;
    Graphics gfx;
    ScrollableContainer container;

    @BeforeEach
    public void setup() {
        context = mock(WidgetContext.class);
        gfx = mock(Graphics.class);
    }

    void dragMouse(int x, int y, Widget widget) {
        widget.handleMouseEvent(new MouseEvent(MouseEvent.Type.DRAGGED, new Position(x, y), 1, 0, Modifier.NONE));
    }

    // Use case 4.11
    @Test
    public void testScrollableContainerHorizontalScroll() {
        Text text = new Text(context, "TEST");
        container = spy(new ScrollableContainer(context, text));
        container.registerScrollBar(Orientation.Horizontal);
        Graphics gfx2 = mock(Graphics.class);
        when(gfx.create(anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(gfx2);
        when(context.getTextSize(any(), anyString())).thenReturn(new Size(500,20));
        container.layout(new Size(100,20));
        container.render(gfx);
        //Step 1: The user slides the scrollbar
        dragMouse(60,2, container.getHorizontalScrollBar());
        //Step 2: The scrollbar has moved to the correct position
        Assertions.assertEquals(container.getHorizontalScrollBar().getOffset(), 51);
    }
    // Use case 4.11
    @Test
    public void testScrollableContainerVerticalScroll() {
        Text text = new Text(context, "TEST");
        container = spy(new ScrollableContainer(context, text));
        container.registerScrollBar(Orientation.Vertical);
        Graphics gfx2 = mock(Graphics.class);
        when(gfx.create(anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(gfx2);
        when(context.getTextSize(any(), anyString())).thenReturn(new Size(20,500));
        container.layout(new Size(100,20));
        container.render(gfx);
        //Step 1: The user slides the scrollbar
        dragMouse(2,10, container.getVerticalScrollBar());
        //Step 2: The scrollbar has moved to the correct position
        Assertions.assertEquals(container.getVerticalScrollBar().getOffset(), 10);
    }
}
