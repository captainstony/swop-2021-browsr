package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class LinearLayoutTest {
    Widget top;
    Widget bottom;
    LinearLayout linearLayout;

    class MockWidget extends Widget {
        public MockWidget(WidgetContext context) {
            super(context);
        }
    }

    Widget buildMockWidget(WidgetContext context, int width, int height) {
        Widget widget = spy(new MockWidget(context));
        when(widget.getSize()).thenReturn(new Size(width, height));
        return widget;
    }

    void buildTestLayout(Orientation direction) {
        WidgetContext context = mock(WidgetContext.class);

        top = buildMockWidget(context, 50, 40);
        bottom = buildMockWidget(context, 50, 20);

        linearLayout = new LinearLayout(context, direction, 5)
                .with(top)
                .with(bottom);
    }

    void buildTestLayoutList() {
        WidgetContext context = mock(WidgetContext.class);

        top = buildMockWidget(context, 50, 40);
        bottom = buildMockWidget(context, 50, 20);

        ArrayList<Widget> list = new ArrayList<>();
        list.add(top);
        list.add(bottom);

        linearLayout = new LinearLayout(context, list, Orientation.Vertical, 10);
    }

    @Test
    void testLayoutVertical() {
        buildTestLayout(Orientation.Vertical);
        linearLayout.layout(new Size(100, 100));
        verify(top).layout(new Size(100, 100 - 5));
        verify(bottom).layout(new Size(100, 60 - 2 * 5));
        Assertions.assertEquals(new Size(50, 40 + 20 + 3 * 5), linearLayout.getSize());
    }

    @Test
    void testLayoutHorizontal() {
        buildTestLayout(Orientation.Horizontal);
        linearLayout.layout(new Size(150, 100));
        verify(top).layout(new Size(150 - 5, 100));
        verify(bottom).layout(new Size(100 - 2 * 5, 100));
        Assertions.assertEquals(new Size(100 + 3 * 5, 40), linearLayout.getSize());
    }

    @Test
    void testRender() {
        buildTestLayoutList(); // Test the other constructor while we are at it
        linearLayout.layout(new Size(100, 150));

        Graphics topGfx = mock(Graphics.class);
        Graphics bottomGfx = mock(Graphics.class);
        Graphics gfx = mock(Graphics.class);
        when(gfx.create(0, 10, 50, 40)).thenReturn(topGfx);
        when(gfx.create(0, 40 + 2 * 10, 50, 20)).thenReturn(bottomGfx);

        linearLayout.render(gfx);
        verify(top).render(topGfx);
        verify(bottom).render(bottomGfx);
    }

    @Test
    void testMouseEventPassing() {
        buildTestLayout(Orientation.Vertical);
        linearLayout.layout(new Size(100, 100));

        MouseEvent mouseEvent = new MouseEvent(MouseEvent.Type.CLICKED, new Position(0, 0), 1, 1, Modifier.NONE);
        linearLayout.handleMouseEvent(mouseEvent);
        MouseEvent mouseEvent1 = new MouseEvent(MouseEvent.Type.CLICKED, new Position(20, 50), 1, 1, Modifier.NONE);
        linearLayout.handleMouseEvent(mouseEvent1);
        MouseEvent mouseEvent2 = new MouseEvent(MouseEvent.Type.CLICKED, new Position(20, 70), 1, 1, Modifier.NONE);
        linearLayout.handleMouseEvent(mouseEvent2);
        verify(top, times(3)).handleMouseEvent(any(MouseEvent.class));
        verify(bottom, times(3)).handleMouseEvent(any(MouseEvent.class));
    }
}
