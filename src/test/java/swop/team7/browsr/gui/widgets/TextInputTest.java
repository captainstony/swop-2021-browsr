package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.BrowserController;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.modals.BrowserModal;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class TextInputTest {
    TextInput input;
    WidgetContext context;
    BrowserController controller;
    BrowserModal modal;

    @BeforeEach
    public void setup() {
        context = mock(WidgetContext.class);
        when(context.getDefaultFont()).thenReturn(new Font(Font.DIALOG, Font.PLAIN, 12));
        when(context.getTextHeight(any(Font.class))).thenReturn(12);
        when(context.getTextSize(any(Font.class), anyString())).thenReturn(new Size(20, 12));
        when(context.getTextOffset(any(Font.class))).thenReturn(5);
        controller = mock(BrowserController.class);
        modal = mock(BrowserModal.class);
        input = spy(new TextInput(context));
        input.setText("Example text");
        input.layout(new Size(500, 500));
        input.handleClickedIn();
    }

    @Test
    public void CursorPositionTest() {
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.HOME, ' ', 0));
        Assertions.assertEquals(input.getCursorPosition(), 0);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 0));
        Assertions.assertEquals(input.getCursorPosition(), 0);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.END, ' ', 0));
        Assertions.assertEquals(input.getCursorPosition(), input.getText().length());
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.RIGHT, ' ', 0));
        Assertions.assertEquals(input.getCursorPosition(), input.getText().length());
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 0));
        Assertions.assertEquals(input.getCursorPosition(), input.getText().length() - 1);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.RIGHT, ' ', 0));
        Assertions.assertEquals(input.getCursorPosition(), input.getText().length());
    }

    @Test
    public void HighLightTest() {
        Assertions.assertEquals((int)input.getHighlightedRange().get(0), 0);
        Assertions.assertEquals((int)input.getHighlightedRange().get(1), input.getText().length());
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.RIGHT, ' ', 64));
        Assertions.assertEquals((int)input.getHighlightedRange().get(1), input.getText().length());
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 64));
        Assertions.assertEquals((int)input.getHighlightedRange().get(1), input.getText().length()-1);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.RIGHT, ' ', 64));
        Assertions.assertEquals((int)input.getHighlightedRange().get(1), input.getText().length());
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 64));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.END, ' ', 64));
        Assertions.assertEquals((int)input.getHighlightedRange().get(0), input.getText().length()-1);
        Assertions.assertEquals((int)input.getHighlightedRange().get(1), input.getText().length());
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.HOME, ' ', 64));
        Assertions.assertEquals((int)input.getHighlightedRange().get(0), 0);
        Assertions.assertEquals((int)input.getHighlightedRange().get(1), input.getText().length()-1);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 64));
        Assertions.assertEquals((int)input.getHighlightedRange().get(0), 0);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.RIGHT, ' ', 64));
        Assertions.assertEquals((int)input.getHighlightedRange().get(0), 1);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 64));
        Assertions.assertEquals((int)input.getHighlightedRange().get(0), 0);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.END, ' ', 64));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 64));
        Assertions.assertFalse(input.isHighlighted());
    }

    @Test
    public void removeCharsTest() {
        String initialText = input.getText();
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.RIGHT, ' ', 0));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.DELETE, ' ', 0));
        Assertions.assertEquals(input.getText(), initialText);
        System.out.println(input.getCursorPosition());
        Assertions.assertEquals(input.getCursorPosition(), initialText.length());
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.HOME, ' ', 0));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.BACKSPACE, ' ', 0));
        Assertions.assertEquals(input.getText(), initialText);
        Assertions.assertEquals(input.getCursorPosition(), 0);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.END, ' ', 0));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.BACKSPACE, ' ', 0));
        Assertions.assertEquals(input.getText(), initialText.substring(0, initialText.length()-1));
        Assertions.assertEquals(input.getCursorPosition(), initialText.length()-1);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 0));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.DELETE, ' ', 0));
        Assertions.assertEquals(input.getText(), initialText.substring(0, initialText.length()-2));
        Assertions.assertEquals(input.getCursorPosition(), initialText.length()-2);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 64));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 64));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.BACKSPACE, ' ', 0));
        Assertions.assertEquals(input.getText(), initialText.substring(0, initialText.length()-4));
        Assertions.assertEquals(input.getCursorPosition(), initialText.length()-4);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 64));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.LEFT, ' ', 64));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.DELETE, ' ', 0));
        Assertions.assertEquals(input.getText(), initialText.substring(0, initialText.length()-6));
        Assertions.assertEquals(input.getCursorPosition(), initialText.length()-6);
    }

    @Test
    public void typeCharsTest() {
        String initialText = input.getText();
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.RIGHT, ' ', 0));
        input.handleKeyEvent(new KeyEvent(401, 0, 'a', 0));
        Assertions.assertEquals(input.getText(), initialText+"a");
        Assertions.assertEquals(input.getCursorPosition(), initialText.length()+1);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.HOME, ' ', 0));
        input.handleKeyEvent(new KeyEvent(401, 0, 'a', 0));
        Assertions.assertEquals(input.getText(), "a"+initialText+"a");
        Assertions.assertEquals(input.getCursorPosition(), 1);
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.RIGHT, ' ', 64));
        input.handleKeyEvent(new KeyEvent(401, KeyEvent.RIGHT, ' ', 64));
        input.handleKeyEvent(new KeyEvent(401, 0, 'a', 0));
        Assertions.assertEquals(input.getText(), "aa"+initialText.substring(2, initialText.length())+"a");
        Assertions.assertEquals(input.getCursorPosition(), 2);
    }
}