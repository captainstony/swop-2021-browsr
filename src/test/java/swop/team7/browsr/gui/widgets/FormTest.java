package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.BrowserController;
import swop.team7.browsr.Context;
import swop.team7.browsr.ControllerTestUtils;

import swop.team7.browsr.Window;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.modals.BrowserModal;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.html.factory.WidgetFactory;
import swop.team7.browsr.html.parser.HtmlParser;
import swop.team7.browsr.html.parser.InvalidBrowsrDocumentException;


import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class FormTest {
    Form form;
    Window window;
    Context context;

    @BeforeEach
    public void setup() {
        window = spy(new Window());
        context = spy(new Context(window));
    }

    @Test
    void testMouseEvent() {
        form = new Form(context,"submit",mock(Widget.class));
        MouseEvent mouseEvent = mock(MouseEvent.class);
        form.handleMouseEvent(mouseEvent);
        verify(form.getChild()).handleMouseEvent(mouseEvent);
    }
}
