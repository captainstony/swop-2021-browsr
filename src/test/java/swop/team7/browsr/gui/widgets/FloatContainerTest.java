package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

import static org.mockito.Mockito.*;

public class FloatContainerTest {
    WidgetContext context;
    Widget child;

    @BeforeEach
    public void setup() {
        context = mock(WidgetContext.class);
        child = mock(Widget.class);
        when(child.getSize()).thenReturn(new Size(50, 70));
    }

    private void runTests(FloatContainer container, int x, int y) {
        container.layout(new Size(500, 500));
        verify(child).layout(new Size(500, 500));

        Graphics gfx = mock(Graphics.class);
        Graphics childGfx = mock(Graphics.class);
        when(gfx.create(x, y, 50, 70)).thenReturn(childGfx);

        container.render(gfx);
        verify(child).render(childGfx);

        container.handleMouseEvent(new MouseEvent(MouseEvent.Type.PRESSED, new Position(200, 300), 1, 1, Modifier.NONE));
        verify(child).handleMouseEvent(argThat((MouseEvent e) -> e.getPosition().equals(new Position(200 - x, 300 - y))));
    }

    @Test
    public void testCenter() {
        FloatContainer container = new FloatContainer(
                context, child, true, true,
                FloatContainer.HorizontalFloatPosition.Center,
                FloatContainer.VerticalFloatPosition.Center
        );

        runTests(container, 250 - 25, 250 - 35);
    }

    @Test
    public void testBottomLeftNoWidthExpand() {
        FloatContainer container = new FloatContainer(
                context, child, false, true,
                FloatContainer.HorizontalFloatPosition.Left,
                FloatContainer.VerticalFloatPosition.Bottom
        );

        runTests(container, 0, 500 - 70);
    }

    @Test
    public void testBottomRight() {
        FloatContainer container = new FloatContainer(
                context, child, true, true,
                FloatContainer.HorizontalFloatPosition.Right,
                FloatContainer.VerticalFloatPosition.Bottom
        );

        runTests(container, 500 - 50, 500 - 70);
    }
}
