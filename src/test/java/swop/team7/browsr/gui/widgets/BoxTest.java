package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

import static org.mockito.Mockito.*;

public class BoxTest {
    @Test
    public void boxTest() {
        WidgetContext context = mock(WidgetContext.class);
        Widget child = mock(Widget.class);
        when(child.getSize()).thenReturn(new Size(50, 70));

        Box box = spy(new Box(context, child, 10, 5, Color.BLACK, Color.YELLOW));

        box.layout(new Size(100, 100));
        verify(child).layout(new Size(100 - 2 * 10, 100 - 2 * 10));
        verify(box).setSize(new Size(50 + 2 * 10, 70 + 2 * 10));

        Graphics gfx = mock(Graphics.class);
        Graphics g2 = mock(Graphics.class);
        Graphics childGfx = mock(Graphics.class);

        when(gfx.create(0, 0, 70, 90)).thenReturn(g2);
        when(gfx.create(10, 10, 50, 70)).thenReturn(childGfx);

        box.render(gfx);

        verify(g2).setColor(Color.BLACK);
        verify(g2).fillRect(0, 0, 70, 90);
        verify(g2).setColor(Color.YELLOW);
        verify(g2).translate(5, 5);
        verify(g2).fillRect(0, 0, 60, 80);

        verify(child).render(childGfx);

        box.handleMouseEvent(new MouseEvent(MouseEvent.Type.PRESSED, new Position(20, 30), 1, 1, Modifier.NONE));
        verify(child).handleMouseEvent(argThat((MouseEvent e) -> e.getPosition().equals(new Position(10, 20))));
    }
}
