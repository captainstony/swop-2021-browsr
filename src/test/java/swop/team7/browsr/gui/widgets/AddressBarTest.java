package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.BrowserController;
import swop.team7.browsr.focus.KeyboardFocusManager;
import swop.team7.browsr.gui.events.KeyEvent;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.modals.BrowserModal;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;
import static org.mockito.Mockito.*;

public class AddressBarTest {
    AddressBar addressBar;
    WidgetContext context;
    BrowserController controller;
    BrowserModal modal;

    @BeforeEach
    public void setup() {
        context = mock(WidgetContext.class);
        when(context.getDefaultFont()).thenReturn(new Font(Font.DIALOG, Font.PLAIN, 12));
        when(context.getTextHeight(any(Font.class))).thenReturn(12);
        when(context.getTextSize(any(Font.class), anyString())).thenReturn(new Size(20, 12));
        when(context.getTextOffset(any(Font.class))).thenReturn(5);
        controller = mock(BrowserController.class);
        modal = mock(BrowserModal.class);
        addressBar = new AddressBar(context);
        addressBar.registerUrlChangedHandler(controller);
        addressBar.layout(new Size(500, 500));
        addressBar.handleMouseEvent(new MouseEvent(500, new Position(5, 5), 1, 1, 0));
        addressBar = spy(addressBar);
    }

    @Test
    public void ClickOutTest() {
        addressBar.handleMouseEvent(new MouseEvent(500, new Position(1000, 1000), 1, 1, 0));
        verify(controller).handleUrlChanged(addressBar.getText());
    }

    @Test
    public void EnterTest() {
        KeyboardFocusManager.sendKeyEventToFocussed(new KeyEvent(401, KeyEvent.ENTER, ' ', 0));
        Assertions.assertFalse(KeyboardFocusManager.isFocussed(addressBar.getTextInput()));
        Assertions.assertFalse(addressBar.getTextInput().isHighlighted());
        verify(controller).handleUrlChanged(addressBar.getText());
    }

    @Test
    public void EscapeTest() {
        String initialText = addressBar.getText();
        KeyboardFocusManager.sendKeyEventToFocussed(new KeyEvent(401, KeyEvent.BACKSPACE, ' ', 0));
        KeyboardFocusManager.sendKeyEventToFocussed(new KeyEvent(401, KeyEvent.ESCAPE, ' ', 0));
        Assertions.assertFalse(KeyboardFocusManager.isFocussed(addressBar));
        Assertions.assertFalse(addressBar.getTextInput().isHighlighted());
        Assertions.assertEquals(addressBar.getText(), initialText);
        verify(controller, never()).handleUrlChanged(anyString());
    }
}
