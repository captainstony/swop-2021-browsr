package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Orientation;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

import static org.mockito.Mockito.*;

public class SplitTest {
    WidgetContext context;
    Graphics gfx;
    Frame left;
    Frame right;
    Split split;

    @BeforeEach
    public void setup() {
        left = mock(Frame.class);
        right = mock(Frame.class);
        context = mock(WidgetContext.class);
        gfx = mock(Graphics.class);
    }

    void dragMouse(int x, int y) {
        split.handleMouseEvent(new MouseEvent(MouseEvent.Type.DRAGGED, new Position(x, y), 1, 0, Modifier.NONE));
    }

    void dragSeparatorHorizontal(int y, int from, int to, int step) {
        if (from < to) {
            for (int current = from; current <= to; current += step) {
                dragMouse(current, y);
            }
        } else {
            for (int current = from; current >= to; current -= step) {
                dragMouse(current, y);
            }
        }
    }

    void dragSeparatorVertical(int x, int from, int to, int step) {
        if (from < to) {
            for (int current = from; current <= to; current += step) {
                dragMouse(x, current);
            }
        } else {
            for (int current = from; current >= to; current -= step) {
                dragMouse(x, current);
            }
        }
    }

    void verifyLayoutRender(int separator, Position firstPos, Position secondPos, Size firstSize, Size secondSize) {
        split.layout(new Size(200, 200));
        Assertions.assertEquals(firstPos, split.getFirstChildPosition());
        Assertions.assertEquals(secondPos, split.getSecondChildPosition());
        Assertions.assertEquals(separator, split.getSeparatorOffset());

        Graphics leftGfx = mock(Graphics.class);
        Graphics rightGfx = mock(Graphics.class);
        when(gfx.create(firstPos.getX(), firstPos.getY(), firstSize.getWidth(), firstSize.getHeight())).thenReturn(leftGfx);
        when(gfx.create(secondPos.getX(), secondPos.getY(), secondSize.getWidth(), secondSize.getHeight())).thenReturn(rightGfx);
        split.render(gfx);
        verify(left).render(leftGfx);
        verify(right).render(rightGfx);
    }

    @Test
    public void testSplitLayoutRenderHorizontal() {
        split = spy(new Split(context, Orientation.Horizontal, left, right));
        when(left.getSize()).thenReturn(new Size(150, 100));
        when(right.getSize()).thenReturn(new Size(200, 200));

        verifyLayoutRender(100,
                new Position(0, 0), new Position(100 + 5, 0),
                new Size(100 - 5, 200), new Size(100 - 5, 200)
        );

        dragSeparatorHorizontal(42, 100, 150, 1);

        verifyLayoutRender(150,
                new Position(0, 0), new Position(150 + 5, 0),
                new Size(150 - 5, 200), new Size(50 - 5, 200)
        );

        // Out of bounds drag
        dragSeparatorHorizontal(42, 150, -50, 1);

        verifyLayoutRender(5,
                new Position(0, 0), new Position(10, 0),
                new Size(0, 200), new Size(200 - 10, 200)
        );
    }

    @Test
    public void testSplitLayoutRenderVertical() {
        split = spy(new Split(context, Orientation.Vertical, left, right));
        when(left.getSize()).thenReturn(new Size(150, 200));
        when(right.getSize()).thenReturn(new Size(100, 200));

        verifyLayoutRender(100,
                new Position(0, 0), new Position(0, 100 + 5),
                new Size(200, 100 - 5), new Size(200, 100 - 5)
        );

        // Drag to edge
        dragSeparatorVertical(42, 100, 200, 1);

        verifyLayoutRender(200 - 5,
                new Position(0, 0), new Position(0, 200),
                new Size(200, 200 - 10), new Size(200, 0)
        );
    }
}
