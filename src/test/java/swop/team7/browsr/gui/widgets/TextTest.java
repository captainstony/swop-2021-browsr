package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;

import static org.mockito.Mockito.*;

public class TextTest {
    @Test
    void textTest() {
        WidgetContext context = mock(WidgetContext.class);
        Color color = Color.DARK_GRAY;
        Font font = context.getDefaultFont();
        when(context.getTextSize(font, "Test")).thenReturn(new Size(40, 20));
        when(context.getTextOffset(font)).thenReturn(3);
        Text text = new Text(context, "Test", font, color);

        Graphics gfx = mock(Graphics.class);

        text.layout(new Size(50, 30));
        Assertions.assertEquals(new Size(40 + 10, 20 + 10), text.getSize());

        text.render(gfx);
        verify(gfx).setColor(color);
        verify(gfx).drawString("Test", 5, 8);
    }
}
