package swop.team7.browsr.gui.widgets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.events.Modifier;
import swop.team7.browsr.gui.events.MouseEvent;
import swop.team7.browsr.gui.util.Position;
import swop.team7.browsr.gui.util.Size;
import swop.team7.browsr.gui.util.WidgetContext;

import java.awt.*;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class TableTest {

    Table table;
    ArrayList<ArrayList<Widget>> cells;

    int rowCount = 10;
    int colCount = 10;
    int cellWidth = 40;
    int cellHeight = 50;

    WidgetContext context = mock(WidgetContext.class);
    class MockWidget extends Widget {
        public MockWidget(WidgetContext style) {
            super(style);
        }
    }

    Widget buildMockWidget(WidgetContext style, int width, int height) {
        Widget widget = spy(new MockWidget(style));
        when(widget.getSize()).thenReturn(new Size(width, height));
        return widget;
    }

    void buildTable() {
        cells = new ArrayList<>();
        for (int i = 0; i < this.rowCount; i++) {
            ArrayList<Widget> list = new ArrayList<>();
            for (int j = 0; j < this.colCount; j++) {
                list.add(buildMockWidget(context,cellWidth,cellHeight));
            }
            cells.add(list);
        }
        this.table = new Table(context,cells);
    }

    Widget getWidgetInTable(int x, int y) {
        if (this.cells != null) {

            int cPosX = 0;
            int cPosY = 0;

            for (ArrayList<Widget> r: table.getMatrix()) {
                for (Widget w : r) { // Widgets retain their size
                    Assertions.assertEquals(w.getSize(), new Size(cellWidth, cellHeight));
                    if (cPosX <= x && x < cPosX+cellWidth) {
                        if (cPosY <= y && y < cPosY+cellHeight) {
                            return w;
                        }
                    }
                    cPosX += cellWidth;
                }
                cPosX = 0;
                cPosY += cellHeight;
            }

        }
        return null;
    }

    @Test
    void testZeroRowTable(){
        Table table = new Table(context,new ArrayList<>());
        table.layout(new Size(1000,1000));
        Assertions.assertEquals(table.getSize(),new Size(0,0)); // Table is empty
    }

    @Test
    void testTableInTable(){
        cells = new ArrayList<>();
        ArrayList<Widget> list = new ArrayList<>();
        list.add(buildMockWidget(context,cellWidth,cellHeight));
        list.add(buildMockWidget(context,cellWidth,cellHeight));

        cells.add(list);
        Table innerTable = new Table(context,cells);

        ArrayList<Widget> row = new ArrayList<>();
        row.add(innerTable);
        ArrayList<ArrayList<Widget>> matrix = new ArrayList<>();
        matrix.add(row);
        row = new ArrayList<>();
        row.add(buildMockWidget(context,cellWidth,cellHeight));
        matrix.add(row);
        Table table = new Table(context,matrix);
        table.layout(new Size(1000,1000));

        /*
        Table should look like:
            ----------------------
            | ------------------- |
            | | Widget | Widget | |
            | ------------------- |
            -----------------------
            |Widget               |
            -----------------------
         */
        Assertions.assertEquals(table.getSize(),new Size(cellWidth*2, cellHeight*2)); // Size includes the inner table
        Assertions.assertEquals(table.getMatrix().get(0).get(0),innerTable); // Element (0,0) of the outer table is the inner table
        Assertions.assertEquals(table.getMatrix().get(1).get(0).getSize(),buildMockWidget(context,cellWidth,cellHeight).getSize()); // Don't forget the other widget in the outer table
    }

    @Test
    void testLayout(){
        this.buildTable();
        this.table.layout(new Size(1000,1000)); // Initialize the table
        Assertions.assertEquals(table.getSize(), new Size(cellWidth*colCount,cellHeight*rowCount)); // Table size is equal to the sum of the children
        for (ArrayList<Widget> r: table.getMatrix()) for(Widget w: r){ // Widgets retain their size
            Assertions.assertEquals(w.getSize(),new Size(cellWidth,cellHeight));
        }
    }

    @Test
    void testRender(){
        this.buildTable();
        this.table.layout(new Size(1000, 1000));
        Graphics gfx = mock(Graphics.class);
        Graphics cellGfx = mock(Graphics.class);

        int cPosX = 0;
        int cPosY = 0;

        for (int i = 0; i < this.rowCount; i++) {
            for (int j = 0; j < this.colCount; j++) {
                when(gfx.create(cPosX, cPosY, cellWidth, cellHeight)).thenReturn(cellGfx);
                cPosX += cellWidth;
            }
            cPosX = 0;
            cPosY += cellHeight;
        }
        this.table.render(gfx);

    }

    @Test
    void testMouseEvent() {
        this.buildTable();
        this.table.layout(new Size(1000, 1000));

        ArrayList<MouseEvent> mouseEvents = new ArrayList<>();
        ArrayList<Position> positions = new ArrayList<>();

        for (int i = 0; i < this.rowCount; i++) {
            for (int j = 0; j < this.colCount; j++) {
                positions.add(new Position(i*cellWidth + cellWidth/2 , j*cellHeight + cellHeight/2 ));
            }
        }

        for (Position p: positions) {
            mouseEvents.add(new MouseEvent(MouseEvent.Type.CLICKED, p,1, 1, Modifier.NONE));
        }

        for (MouseEvent m: mouseEvents) {
            table.handleMouseEvent(m);
        }

        for (Position p: positions) {
            Widget w = this.getWidgetInTable(p.getX(), p.getY());
            if (w != null) {
                verify(w, times(colCount*rowCount)).handleMouseEvent(any(MouseEvent.class));
            }
        }

    }
}
