package swop.team7.browsr.gui.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.awt.*;

public class PositionTest {
    @Test
    public void testPosition() {
        Position pos = new Position(6, 8);
        Position otherPos = new Position(6, 8);

        Assertions.assertEquals(otherPos, pos);
        Assertions.assertEquals(new Rectangle(6, 8, 9, 3), pos.toRectangle(new Size(9, 3)));
    }
}
