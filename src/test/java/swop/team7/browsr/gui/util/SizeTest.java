package swop.team7.browsr.gui.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.awt.*;

public class SizeTest {
    @Test
    public void testSize() {
        Size first = new Size(5, 5);
        Size second = new Size(69, 42);

        Assertions.assertEquals(second, first.setWidth(69).setHeight(42));

        Assertions.assertEquals(new Rectangle(3, 2, 5, 5), first.toRectangle(new Position(3, 2)));
    }
}
