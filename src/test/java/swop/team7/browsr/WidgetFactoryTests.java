package swop.team7.browsr;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import swop.team7.browsr.gui.widgets.*;
import swop.team7.browsr.gui.widgets.Button;
import swop.team7.browsr.html.factory.WidgetFactory;
import swop.team7.browsr.html.parser.HtmlParser;
import swop.team7.browsr.html.parser.InvalidBrowsrDocumentException;
import swop.team7.browsr.html.parser.objects.*;

import java.awt.*;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class WidgetFactoryTests {

    Window window;
    BrowserController controller;
    Graphics graphics;
    FontMetrics metrics;
    Form form;
    @BeforeEach
    public void setup() {
        window = spy(new Window());
        Context context = new Context(window);
        graphics = mock(Graphics.class);
        metrics = mock(FontMetrics.class);

        when(metrics.stringWidth(any())).thenReturn(30);
        when(metrics.getHeight()).thenReturn(20);
        when(metrics.getAscent()).thenReturn(5);

        when(graphics.create(anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(graphics);
        when(graphics.getFontMetrics()).thenReturn(metrics);

        doAnswer(invocationOnMock -> {
            window.paint(graphics);
            return null;
        }).when(window).requestRedraw();

        doReturn(metrics).when(window).getFontMetrics(any());
        doReturn(800).when(window).getWidth();
        doReturn(500).when(window).getHeight();

        window.handleShown();
        controller = spy(new BrowserController(window, context));
    }

    //Generate form from raw string
    public void setForm(String rawForm){
        WidgetFactory factory = new WidgetFactory(controller);
        HtmlParser parser = new HtmlParser(rawForm);
        try {
            form = (Form) factory.createWidget(parser.parse());
        } catch (InvalidBrowsrDocumentException e) {
            assert false;
        }
    }
    @Test
    public void CorrectCreationAdvanced() {
        String rawForm = """
                <form action="browsrformactiontest.php">
                  <table>
                    <tr><td>List words from the Woordenlijst Nederlandse Taal
                    <tr><td>
                      <table>
                        <tr><td>Words that start with<td><input type="text" name="starts_with">
                        <tr><td>Maximum number of words to show<td><input type="text" name="max_nb_results">
                      </table>
                    <tr><td><input type="submit">
                  </table>
                </form>
                """;
        setForm(rawForm);
        //Test if the create form is an actual Form
        Assertions.assertEquals(form.getClass(), Form.class);
        //Form child should be a table
        Assertions.assertEquals(form.getChild().getClass(),Table.class);
        //Last element in the table should be a button
        Assertions.assertEquals(((Table) form.getChild()).getMatrix().get(2).get(0).getClass(), swop.team7.browsr.gui.widgets.Button.class);
        //Both text input fields should be present (this is more of a table test)
        Assertions.assertEquals(((Table) ((Table) form.getChild()).getMatrix().get(1).get(0)).getMatrix().get(0).get(1).getClass(),InputField.class);
        Assertions.assertEquals(((Table) ((Table) form.getChild()).getMatrix().get(1).get(0)).getMatrix().get(1).get(1).getClass(),InputField.class);
    }

    @Test
    public void CorrectCreationSimple() {
        String rawForm = """
                <form action="test.php">
                  <table>
                    <tr><td><input type="text" name="starts_with">
                    <tr><td><input type="text" name="max_nb_results">
                    <tr><td><input type="submit">
                  </table>
                </form>
                """;
        setForm(rawForm);
        //Test if the create form is an actual Form
        Assertions.assertEquals(form.getClass(),Form.class);
        //Form child should be a table
        Assertions.assertEquals(form.getChild().getClass(),Table.class);
        //Last element in the table should be a button
        Assertions.assertEquals(((Table) form.getChild()).getMatrix().get(2).get(0).getClass(), swop.team7.browsr.gui.widgets.Button.class);
        swop.team7.browsr.gui.widgets.Button button = ((Button) ((Table) form.getChild()).getMatrix().get(2).get(0));
        //Both text input fields should be present (this is more of a table test)
        Assertions.assertEquals(((Table) form.getChild()).getMatrix().get(0).get(0).getClass(),InputField.class);
        InputField inputField1 = ((InputField) ((Table) form.getChild()).getMatrix().get(0).get(0));
        Assertions.assertEquals(((Table) form.getChild()).getMatrix().get(1).get(0).getClass(),InputField.class);
        InputField inputField2 = ((InputField) ((Table) form.getChild()).getMatrix().get(1).get(0));
    }
    @Test
    void testSingleWidgets(){
        WidgetFactory widgetFactory = new WidgetFactory(controller);
        Widget root = widgetFactory.createWidget(new HtmlA("HREF",new HtmlText("TEXT")));
        Assertions.assertEquals(root.getClass(),Link.class);
        Assertions.assertEquals(((Link) root).getText(),"TEXT");
        Assertions.assertEquals(((Link) root).getTarget(),"HREF");
        root = widgetFactory.createWidget(new HtmlTable(new ArrayList<>()));
        Assertions.assertEquals(root.getClass(), Table.class);
        root = widgetFactory.createWidget(new HtmlText("TEXT"));
        Assertions.assertEquals(((Text) root).getText(),"TEXT");
        Assertions.assertEquals(root.getClass(), Text.class);
        root = widgetFactory.createWidget(new HtmlTr(new ArrayList<>()));
        Assertions.assertEquals(root,null);
        root = widgetFactory.createWidget(new HtmlTd(null));
        Assertions.assertEquals(root,null);
    }

    @Test
    void testBasicTableCreation() throws Exception{
        WidgetFactory widgetFactory = new WidgetFactory(controller); // From this point we assume HtmlParser works correctly
        HtmlParser parser = new HtmlParser("""
                <table>
                  <tr> <td> test
                </table>
                """);
        Widget root = widgetFactory.createWidget(parser.parse());
        Assertions.assertEquals(root.getClass(),Table.class);
        Assertions.assertEquals(((Table) root).getMatrix().size(), 1);
        Assertions.assertEquals(((Table) root).getMatrix().get(0).size(), 1);
        Assertions.assertEquals(((Table) root).getMatrix().get(0).get(0).getClass(),Text.class);
    }

    @Test
    void testEmptyTableCreation() throws Exception{
        WidgetFactory widgetFactory = new WidgetFactory(controller); // From this point we assume HtmlParser works correctly
        HtmlParser parser = new HtmlParser("""
                <table>
                </table>
                """);
        Widget root = widgetFactory.createWidget(parser.parse());
        Assertions.assertEquals(root.getClass(),Table.class);
        Assertions.assertEquals(((Table) root).getMatrix().size(), 0);
    }

    @Test
    void testEmptyRowTableCreation() throws  Exception{
        WidgetFactory widgetFactory = new WidgetFactory(controller); // From this point we assume HtmlParser works correctly
        HtmlParser parser = new HtmlParser("""
                <table>
                  <tr>
                </table>
                """);
        Widget root = widgetFactory.createWidget(parser.parse());
        Assertions.assertEquals(root.getClass(),Table.class);
        Assertions.assertEquals(((Table) root).getMatrix().size(), 1);
        Assertions.assertEquals(((Table) root).getMatrix().get(0).size(), 0);
    }

    @Test
    void testTableInTableCreation() throws  Exception{
        WidgetFactory widgetFactory = new WidgetFactory(controller); // From this point we assume HtmlParser works correctly
        HtmlParser parser = new HtmlParser("""
                <table>
                  <tr> <td> <table>
                              <tr> <td> test
                            </table>
                </table>
                """);
        Widget root = widgetFactory.createWidget(parser.parse());
        Assertions.assertEquals(root.getClass(),Table.class);
        Assertions.assertEquals(((Table) root).getMatrix().size(), 1);
        Assertions.assertEquals(((Table) root).getMatrix().get(0).size(), 1);
        Assertions.assertEquals(((Table) root).getMatrix().get(0).get(0).getClass(), Table.class); // Nested element is a table.
        Assertions.assertEquals(((Table) ((Table) root).getMatrix().get(0).get(0)).getMatrix().size(), 1);
        Assertions.assertEquals(((Table) ((Table) root).getMatrix().get(0).get(0)).getMatrix().get(0).size(), 1);
        Assertions.assertEquals(((Table) ((Table) root).getMatrix().get(0).get(0)).getMatrix().get(0).get(0).getClass(), Text.class);

    }
}
