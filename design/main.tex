
\title{Browsr: Design}
\author{
    Andreas Devogel \and
    Daan Vanoverloop \and
    Dmitry Buggenhout \and
    Tom Van Steenbergen
}
\date{\today}

\documentclass[11pt,a4paper]{article}
\usepackage[a4paper, margin=1in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}

\begin{document}
\maketitle

\section{Controllers, Handlers, Modals and Widgets}

A Modal is a special type of Widget that takes all the space of the window and receives all input events.
The Window contains a stack of Modals and will only show the Modal on the top of the stack at one time.
Only this Modal will receive input events.
Controllers can create Modals and push them on this stack. Modals consist of different Widgets.
Widgets can send system events to Controllers by using Handlers. 
This is a listener pattern that creates low coupling between Controllers and Widgets. 
Controllers usually use their Modal to access some relevant Widgets to view or change the state of the GUI.
For more information, refer to \texttt{diagrams/overview\_main.png}, 
\texttt{diagrams/overview\_controllers.png}, \texttt{diagrams/gui.png} and \texttt{diagrams/gui\_widgets.png}.

\section{The Window and Widgets in their Context}

The Window receives input events. These events are sent to the Modal. However, Widgets have no knowledge
of the Window. They can get information, like font sizes, and request redraws by using the WidgetContext
interface. This interface is implemented by the Context class which is closely related to the Window.
This choice was made to reduce coupling between the Widgets and the Window. It allows Widgets to exist within
another context.

\section{Widget Lifecycle}

Each Widget implements four methods: layout, render, handleMouseEvent and handleKeyEvent.

\begin{enumerate}
    \item Layout Phase. In this phase, each Widget is responsible
        for calling the layout method on its children with an appropriate maximum size, after which it must set its own size.
    \item Render Phase. After the layout phase Widgets can be drawn to the screen. They may use the provided Graphics object
        to draw themselves. They must also call the render method on their children with a clipped and translated version
        of the Graphics object. All coordinates are relative to the position of each Widget.
    \item Input events can now be processed by the Widgets. They must propagate mouse events to their children and
        they must make sure the position of the mouse event is translated to the relative position of their children.
        Key events are first processed by the Modal Widget. The Modal will then use the FocusManager to send the key event
        to the focussed Widget.
\end{enumerate}

When Widgets respond to input events they may wish to request another call of layout or render. They can use the WidgetContext
for this, after which these method calls will again be propagated through the entire Widget Tree. This is due to a
limitation with the CanvasWindow library, which does not enable us to make partial redraws of the Widget Tree.

\section{Widget Tree Layout}

To achieve low coupling and high cohesion, the widget tree is built from many simple widgets
that each have their own specific job. These widgets can be nested within each other to create
different layouts. In the section we will show some diagrams that clarify how each modal is built.

For example, in figure~\ref{browsermodal} BrowserModal uses a LinearLayout widget to stack the AddressBar, BookmarkBar
and the Document View on top of each other. A ScrollableContainer is used to handle all scrolling logic, such
that neither the Frame widget nor the HTML Root Widget must know about this concept. This scrolling widget
is also used in the TextInputWidget (see figure~\ref{inputwidget}. 
That way scrolling logic is only implemented in a single place.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{browsermodal}
\caption{Layout of the BrowserModal}
\label{browsermodal}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{inputwidget}
\caption{TextInputWidget}
\label{inputwidget}
\end{figure}

Another example can be seen in figure~\ref{savemodal}. Different Container-like and LinearLayout widgets are
used by the Dialog widget to create a dialog window. The title, content and buttons are provided by the SaveModal.
The Dialog widget is responsible for creating the correct layout.
Some of these widgets, like Text, Button and InputField, are also used within the Document View.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{savemodal}
\caption{Layout of the SaveModal}
\label{savemodal}
\end{figure}

\end{document}
